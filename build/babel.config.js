// process.env.NODE_ENV
// plugins: ['transform-remove-console']
let plugins = []
if (process.env.NODE_ENV === "pro") {
  plugins.push("transform-remove-console")
}
module.exports = {
  plugins,
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
}
