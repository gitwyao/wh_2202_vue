export default {
    // 开启命名空间  (如果是获取state不需要添加namespaced)
    namespaced: true,
    state: {
        name: "张三"
    },
    getters: {
        newCount() {
            return 111111111
        }
    },
    mutations: {
        changeName(state) {
            state.name = "张三修改了"
        }
    },
    actions: {
        init() {
            console.log('张三中的init')
        }
    }
}