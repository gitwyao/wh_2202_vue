export default {
    // 开启命名空间
    namespaced: true,
    state: {
        name: "李四"
    },
    getters: {
        newCount() {
            return 2222222222
        }
    },
    mutations: {
        changeName(state) {
            state.name = "李四修改了"
        }
    }
}