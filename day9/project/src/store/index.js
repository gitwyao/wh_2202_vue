import Vue from 'vue'
import Vuex from 'vuex'

import state from './state'

// 引入模块
import Zhang from './modules/Zhang'
import Li from './modules/Li'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count:  888,
    num: 999
  },
  getters: {
    newCount(state) {
      return state.count + '!!!!!!!!!'
    }
  },
  mutations: {
    fn() {
      console.log("我是mutations中的方法")
    }
  },
  actions: {
    init() {
      console.log("我是actions中的方法")
    }
  }, 
  // 模块划分
  modules: {
    Zhang,
    // Li
  }
})


// 第二种写法
// export default new Vuex.Store({
//   state,
//   getters,
//   mutations,
//   actions,
//   modules
// })

