import axios from 'axios'
import NproGress from 'nprogress'
import { Message } from 'element-ui'
import 'nprogress/nprogress.css'

// 设置公共地址
// axios.defaults.baseURL = "https://api-hmugo-web.itheima.net/api/public/v1"
// 设置请求时间
// axios.defaults.timeout = 30000


// 第二种写法
// http://aaaaaaaaaaaaaa
// http://bbbbbbbbbbbbbb
// http://cccccccccccccc

// let aaa = axios.create({
//     baseURL: "https://api-hmugo-web.itheima.net/api/public/v1"
// })
// let bbb = axios.create({
//     baseURL: "http://bbbbbbbbbbbbbb"
// })
// let ccc = axios.create({
//     baseURL: "http://aaaaaaaaaaaaaa"
// })

// 发起请求两个过程
// 1. 请求前  (请求拦截)
axios.interceptors.request.use((config) => {
    // config  本次请求接口的信息
    // console.log('我现在发起请求--被拦截了')
    console.log(config)

    NproGress.start()
    return config
})

// 2. 响应后   (响应拦截)
axios.interceptors.response.use((data) => {
    // console.log('我现在响应到了--被拦截了')
    // console.log(data)

    // 根据接口的状态来判断
    console.log(data.data)
    switch (data.data.status) {
        case 200:
            Message({
                message: data.data.msg,
                type: 'success'
            });
            break;
        case 401:
            Message({
                message: data.data.msg,
                type: 'warning'
            });
            break;
        case 500:
            // Message.error(data.data.msg)
            Message({
                message: data.data.msg,
                type: 'error'
            });
            break;
        // default:
    }

    NproGress.done()
    // 方便以后读取数据
    return data.data.message
})

export default axios