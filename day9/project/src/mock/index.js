import Mock from 'mockjs'

Mock.setup({
    // 模拟请求延时
    timeout: "1000"
})

Mock.mock("/getList", {
    message: [111, 222, 333]
})


Mock.mock("/getInfo", {
    message: [666, 777, 888]
})


Mock.mock("/one", {
    message: "第一个接口",
    status: 200,
    msg: "请求成功"
})


Mock.mock("/two", {
    message: "第二个接口",
    status: 401,
    msg: "你还有没有登录"
})

Mock.mock("/three", {
    message: "three",
    status: 500,
    msg: "服务端错误"
})