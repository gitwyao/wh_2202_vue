import axios from 'axios'
import NproGress from 'nprogress'
import { Message } from 'element-ui'
import 'nprogress/nprogress.css'

// 设置公共地址
axios.defaults.baseURL = "/api"


// 请求拦截
axios.interceptors.request.use((config) => {
    // 开启
    NproGress.start()
    return config
})

// 响应拦截
axios.interceptors.response.use(({ data }) => {
    switch(data.code) {
        case 200:
            Message({
                message: data.msg,
                type: "success"
            })
            break; 
    }

    // 关闭
    NproGress.done()
    return data.data
})

export default axios