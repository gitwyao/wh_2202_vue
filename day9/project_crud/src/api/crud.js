import axios from '@/http/request'

// 获取列表
function getList() {
    return axios.get("/getList")
}

// 删除
function delItem(params) {
    return axios.get("/detailItem", {
        params
    })
}

export {
    getList,
    delItem
}