import axios from '@/http/request'
import { getList, delItem } from '@/api/crud'

export default {
    namespaced: true,
    state: {
        tableData: []
    },
    mutations: {
        setTableData(state, list) {
            state.tableData = list
        }
    },
    actions: {
        // 初始化
        init({ commit }) {
            // axios.get("/getList").then(res => {
            //     commit("setTableData", res)
            // })

            getList().then(res => {
                commit("setTableData", res)
            })
        },
        // 删除
        delItem({ commit }, id) {
            // axios.get("/detailItem", {
            //     params: {
            //         id
            //     }
            // }).then(res => {
            //     commit("setTableData", res)
            // })

            delItem({ id }).then(res => {
                commit("setTableData", res)
            })
        },
        // 新增
        addItem({ commit }, obj) {
            // console.log(JSON.stringify(obj))
            axios.get("/addItem", {
                params: {
                    ...obj
                }
            }).then(res => {
                commit("setTableData", res)
            })
        }
    }
}