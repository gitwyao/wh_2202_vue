import Vue from 'vue'
import Vuex from 'vuex'
import Crud from './modules/crud'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    Crud
  }
})
