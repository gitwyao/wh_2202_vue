import Mock from 'mockjs'

Mock.setup({
    timeout: "500-1000"
})

let data = [
    {
        id: 1234,
        name: "张三",
        address: "北京",
        age: 20
    },
    {
        id: 2345,
        name: "李四",
        address: "上海",
        age: 25
    },
    {
        id: 3456,
        name: "王五",
        address: "深圳",
        age: 18
    }
]

// 获取表格数据
Mock.mock("/api/getList", {
    data,
    code: 200,
    msg: "请求成功"
})

// 删除
Mock.mock(RegExp('/api/detailItem' + '.*'), (req) => {
    const id = Number(req.url.split("=")[1])
    let filterList = data.filter(item => item.id !== id)
    return {
        data: filterList,
        code: 200,
        msg: "删除成功"
    }
})

// 新增
Mock.mock(RegExp("/api/addItem" + '.*'), (req) => {
    console.log(req)
    const name = req.url.split('&')[0].split('=')[1]
    const address = req.url.split('&')[1].split('=')[1]
    const age = req.url.split('&')[2].split('=')[1]
    const id = req.url.split('&')[3].split('=')[1]

    let info = {
        name,
        address,
        age,
        id
    }
    data.push(info)
    return {
        data,
        code: 200,
        msg: "添加成功"
    }
})