import axios from 'axios'
import { Toast } from 'vant'

// 配置公共地址
axios.defaults.baseURL = "http://baojia.chelun.com"
axios.defaults.timeout = 3000

// 请求前拦截
axios.interceptors.request.use((config) => {
    Toast.loading({
        message: '加载中...',
        forbidClick: true
    })
    return config
})

// 响应拦截
axios.interceptors.response.use((data) => {
    Toast.clear()
    return data
})

export default axios