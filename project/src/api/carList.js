import axios from '@/utils/request'

// 汽车品牌数据
function getMasterBrandList() {
    return axios.get("/v2-car-getMasterBrandList.html")
}

// 品牌车系数据
function getMakeListByMaster(params) {
    return axios.get("/v2-car-getMakeListByMasterBrandId.html", {
        params
    })
}

// 汽车车系详情
function getInfoAndList(params) {
    return axios.get("/v2-car-getInfoAndListById.html", {
        params
    })
}

// 获取汽车颜色列表
function getImageColor(params) {
    return axios.get("/v2-car-getModelImageYearColor.html", {
        params
    })
}

// 获取车系图片列表
function getImageList(params) {
    return axios.get("/v2-car-getImageList.html", {
        params
    })
}

// 自动定位
function getClient(params) {
    return axios.get("/location-client.html", {
        params
    })
}

// 经销商列表
function getDealerAlllist(params) {
    return axios.get("/v2-dealer-alllist.html", {
        params
    })
}

// 获取全国城市
function getCityAlllist(params) {
    return axios.get("/v1-city-alllist.html", {
        params
    })
}

// 获取车系图片分类列表
function getCategoryImageList(params) {
    return axios.get("/v2-car-getCategoryImageList.html", {
        params
    })
}


export {
    getMasterBrandList,
    getMakeListByMaster,
    getInfoAndList,
    getImageColor,
    getImageList,
    getClient,
    getDealerAlllist,
    getCityAlllist,
    getCategoryImageList
}