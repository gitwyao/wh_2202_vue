import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant, { Lazyload } from 'vant'
import VConsole from 'vconsole'
import 'amfe-flexible'
import './utils/rem'
import 'vant/lib/index.css'

// console.log(process.env.NODE_ENV)

// if (process.env.NODE_ENV === "development") {
//   const Console = new VConsole()
//   Vue.use(Console)
// }
Vue.use(Vant)
Vue.use(Lazyload)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

