import Vue from 'vue'
import Vuex from 'vuex'
import CarList from './modules/CarList'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    CarList
  }
})
