function filterRoutes(arr, auth) {
    return arr.filter(item => item.meta.auth.indexOf(auth) !== -1)
}

export {
    filterRoutes
}