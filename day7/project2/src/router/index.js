import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import NotFind from '../views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "/login"
  },
  {
    path: "/login",
    component: Login,
    // meta  自定义字段
  },
  {
    path: "*",
    component: NotFind
  }
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
