import Home from '../../views/Home'
import About from '../../views/About'
import List from '../../views/List'

export default [
    {
        path: "/home",
        component: Home,
        meta: {
            auth: ["管理员", "老师"]
        },
        // children: [
        //     {
        //         path: "/home/one", 
        //         meta: {
        //             auth: ["管理员", "老师"]
        //         },
        //     },
        //     {
        //         path: "/home/two", 
        //         meta: {
        //             auth: ["管理员"]
        //         },
        //     }
        // ]
    },
    {
        path: "/about",
        component: About,
        meta: {
            auth: ["管理员"]
        }
    },
    {
        path: "/list",
        component: List,
        meta: {
            auth: ["管理员", "老师", "学生"]
        }
    }
]