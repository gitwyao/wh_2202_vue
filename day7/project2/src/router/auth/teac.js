import Home from '../../views/Home'
import List from '../../views/List'

export default [
    {
        path: "/home",
        component: Home
    },
    {
        path: "/list",
        component: List
    }
]