import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login'
import Detail from '../views/Detail'

import Homes from '../views/home/Homes'
import List from '../views/home/List'
import My from '../views/home/My'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        // path值如果不加 /  会和父级自动拼接
        path: 'homes',
        component: Homes
      },
      {
        path: 'list',
        component: List,
        name: "List",
        // beforeEnter: (to, from, next) => {
        //   console.log('进入list，单独拦截')
        //   // next()
        // }
      },
      {
        path: 'my',
        component: My
      }
    ]
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/detail',
    component: Detail
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 路由拦截 
// 1. 全局拦截 

// 进入之前进行拦截

// router.beforeEach((to, from, next) => {
//   // console.log(to)
//   // console.log(from)
//   // next 执行下一步

//   if (to.path === "/home/my") {
//     // 没有token  说明没有登录
//     if (!sessionStorage.getItem('token')) {
//       // next可以传递路径，指定跳转
//       next("/login")
//     }
//     console.log('准备进行拦截')
//   }
//   next()
// })

// 进入之后拦截
// router.afterEach((to, from) => {
//   console.log(to, from)
// })

// router.beforeResolve

// 2. 独享拦截

// 3. 组件内部拦截


export default router
