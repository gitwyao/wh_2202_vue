import Mock from "mockjs";

const data = [
    {
        id: 111,
        title: "商品1",
        src: "https://img0.baidu.com/it/u=2862534777,914942650&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500"
    },
    {
        id: 222,
        title: "商品2",
        src: "https://img1.baidu.com/it/u=1966616150,2146512490&fm=253&fmt=auto&app=138&f=JPEG?w=751&h=500"
    },
    {
        id: 333,
        title: "商品3",
        src: "https://img1.baidu.com/it/u=2335185831,1224854927&fm=253&fmt=auto&app=138&f=JPG?w=753&h=500"
    }
]


Mock.mock(RegExp("/detail" + ".*"), (req) => {
    const id = req.url.split("=")[1]
    return data.filter(item => item.id === Number(id))[0]
})