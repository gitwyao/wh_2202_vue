import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  layout: {
    title: "后台管理",
    logo: 'https://img1.baidu.com/it/u=3249002328,1077948853&fm=253&fmt=auto&app=138&f=JPEG?w=499&h=258',
  },
  // 路由
  // 约定式
  // 声明式
  routes: [
    { path: '/', component: '@/pages/index' },
    { 
      name: "列表",
      path: '/list', 
      component: '@/pages/list',
      wrappers: [
        '@/wrappers/auth'
      ],
      routes: [
        {
          name: "One",
          path: 'one', 
          component: '@/pages/list/one',
        },
        {
          name: "Two",
          path: 'two', 
          component: '@/pages/list/two',
        },
      ]
    },
    { 
      name: "测试",
      path: '/about', 
      component: '@/pages/about'
    },
    {
      path: "/login",
      component: "@/pages/Login",
      // 不让头部内容展示
      headerRender: false,
      footerRender: false,
      menuRender: false
    }
  ],
  // 开启dva
  dva: {
    immer: true,
    hmr: true
  },
  fastRefresh: {},
});
