import React from 'react'
import { history } from 'umi'

type Props = {}

export default function Login({ }: Props) {
    const login = () => {
        history.push('/list')
    }
    return (
        <div>
            Login
            <button onClick={login}>登录</button>
        </div>
    )
}