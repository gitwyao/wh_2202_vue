import React, { ReactNode } from 'react'
import { Redirect } from 'umi'
import { useAuth } from '@/hooks/useAuth'

type Props = {
    children: ReactNode
}

export default function auth({ children }: Props) {
    const { isLogin } = useAuth()
    console.log(isLogin)
    if (isLogin) {
        return <div>{ children }</div>;
      } else {
        return <Redirect to="/login" />;
      }
}