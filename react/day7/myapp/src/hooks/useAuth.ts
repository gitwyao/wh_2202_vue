function useAuth() {
    const isLogin = sessionStorage.getItem('token')? true: false
    return {
        isLogin
    }
}

export {
    useAuth
}