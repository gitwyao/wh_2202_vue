import React, { useRef, useEffect } from 'react'

export default function Ref() {
    const myh = useRef()
    useEffect(() => {
        console.log(myh.current)
    }, [])
    return (
        <div>
            <h3 ref={myh}>我是标题</h3>
        </div>
    )
}
