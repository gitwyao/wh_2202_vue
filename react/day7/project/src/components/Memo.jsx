import React, { useMemo, useState } from 'react'

// useCallback  缓存函数    
// useMemo      缓存值      当前组件中缓存值

export default function Memo() {
    const [count, setCount] = useState(100)
    const [allPrice, setAll] = useState(1)

    const newCount = () => {
        console.log('count重新执行了')
        return '$' + count + '.00'
    }


    const newPrice = useMemo(() => {
        console.log('price重新执行了')
        return '$$$$' + allPrice + '.00'
    }, [allPrice])
    return (
        <div>
            <p>
                数量: {newCount()}
            </p>
            <button onClick={() => {
                setCount(count + 1)
            }}>数量+1</button>

            <p>
                价格: {newPrice}
            </p>
            <button onClick={() => {
                setAll(allPrice + 1)
            }}>价格+1</button>
        </div>
    )
}
