import React from 'react'
import { Select } from 'antd'
import { useOptions } from '../hooks'
const { Option } = Select

export default function MySelect({ title, api, id, keys, defaultValue, acceptOption }) {
    const list = useOptions(api)

    const handleChange = (value) => {
        acceptOption(keys, value)
    };
    return (
        <div>
            <span>
                请求选择{title}类型：
            </span>
            <Select
                defaultValue={defaultValue}
                style={{
                    width: 120,
                }}
                onChange={handleChange}
            >
                {
                    list.map(item => {
                        return <Option key={item[id]} value={item[id]}>
                            {item[keys]}
                        </Option>
                    })
                }
            </Select>
        </div>
    )
}
