import React, { useCallback, useState, useEffect } from 'react'

// useCallback  缓存函数

function Callback() {
    const [count, setCount] = useState(100)
    // 不使用 useCallback 的情况  
    const add = () => {
        setCount(count + 1)
    }

    const callBackAdd = useCallback(() => {
        setCount(888)
    }, [])

    return (
        <div>
            Callback --- { count }
            <Son add = { add }  callBackAdd = { callBackAdd }/>
        </div>
    )
}

function Son({ add, callBackAdd }) {
    useEffect(() => {
        console.log('没有使用userCallback')
    }, [add])

    useEffect(() => {
        console.log('使用看userCallback--callBackAdd')
    }, [callBackAdd])
    return <div>
        <h5>我是子组件</h5>
        <button onClick={ add }>点我+</button>

        <button onClick={ callBackAdd }>点我+2</button>
    </div>
}


export default Callback
