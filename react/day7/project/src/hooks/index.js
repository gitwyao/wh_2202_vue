import { useEffect, useState } from 'react'
import axios from 'axios'

function useOptions(url) {
    const [list, setList] = useState([])
    useEffect(() => {
        axios.get(url).then(res => {
            setList(res.data.list)
        })
    }, [])
    return list
}

export {
    useOptions
}