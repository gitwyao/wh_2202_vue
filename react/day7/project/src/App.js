import MySelect from './components/MySelect';
import Callback from './components/Callback';
import Memo from './components/Memo';
import Ref from './components/Ref';
import { Button } from 'antd'
import './App.css';

function App() {
  // let examInfo = {}
  // const sub = () => {
  //   console.log(examInfo)
  // }
  // const fn = (key, value) => {
  //   examInfo[key] = value
  // }
  return (
    <div className="App">
      {/* <MySelect acceptOption={fn} defaultValue="周考一" keys="exam_name" id="exam_id" api="/exam/getExamType" title="考试"/>
      <MySelect acceptOption={fn} defaultValue="js上" keys="subject_text" id="subject_id" api="/exam/subjectType" title="课程"/>
      <MySelect acceptOption={fn} defaultValue="简答题" keys="questions_type_text" id="questions_type_id" api="/exam/questionsType" title="题目"/>

      <Button onClick={ sub }>提交</Button> */}

      {/* <Callback/> */}

      {/* <Memo/> */}

      <Ref/>
    </div>
  );
}

export default App;
