import Mock from 'mockjs'

Mock.mock('/exam/getExamType', {
    list: [
        { "exam_id": "8sc5d7-7p5f9e-cb2zii-ahe5i", "exam_name": "周考1" },
        { "exam_id": "jpg8y9-zbzt7o-jpvuhf-fwnjvr", "exam_name": "周考2" },
        { "exam_id": "ukmp9b-radddj-ogwdr-nw3ane", "exam_name": "周考3" },
        { "exam_id": "wbxm4-jf8q6k-lvt2ca-ze96mg", "exam_name": "月考" }]
})

Mock.mock('/exam/subjectType', {
    list: [{ "subject_id": "fqtktr-1lq5u", "subject_text": "javaScript上" }, { "subject_id": "wl5yk-38c0g", "subject_text": "javaScript下" }, { "subject_id": "8tl7os-r49tld", "subject_text": "模块化开发" }, { "subject_id": "1ux00o6-2xbj5i", "subject_text": "移动端开发" }, { "subject_id": "4pu32-vs796l", "subject_text": "node基础" }, { "subject_id": "1psw2b-cy7o07", "subject_text": "组件化开发(vue)" }, { "subject_id": "fyu3ln-azjkie", "subject_text": "渐进式开发(react)" }, { "subject_id": "94sjh6-lnlxe", "subject_text": "项目实战" }, { "subject_id": "k1gvd4-8lrx8f", "subject_text": "javaScript高级" }, { "subject_id": "u3ix15-dd6md", "subject_text": "node高级" }]
})


Mock.mock('/exam/questionsType', {
    list: [
        { "questions_type_id": "774318-730z8m", "questions_type_text": "简答题", "questions_type_sort": 1 },
        { "questions_type_id": "br9d6s-wh46i", "questions_type_text": "代码阅读题", "questions_type_sort": 2 },
        { "questions_type_id": "fwf0t-wla1q", "questions_type_text": "代码补全", "questions_type_sort": 3 },
        { "questions_type_id": "cll4tv-kt4ppt", "questions_type_text": "修改bug", "questions_type_sort": 4 },
        { "questions_type_id": "7lwm3-2tvqsg", "questions_type_text": "456", "questions_type_sort": 11271 }]
})