import { NavLink, Route } from 'react-router-dom'
import myLoadAble from './utils/loadAble' 

// import Home from './pages/HomeAble'

// import About from './pages/About';
// import Home from './pages/Home';
import './App.css';

const Home = myLoadAble(() => import('./pages/Home'))
const About = myLoadAble(() => import('./pages/About'))

function App() {
  return (
    <div className="App">
      <NavLink to="/home">首页</NavLink>--
      <NavLink to="/about">测试</NavLink>

      <Route path="/home" component={ Home }/>
      <Route path="/about" component={ About }/>
    </div>
  );
}

export default App;
