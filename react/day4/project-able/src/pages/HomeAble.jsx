import Loadable from "react-loadable";

const MyLoadingComponent = () => {
    return <div>
        加载中~~~
    </div>
}

const AsyncHome = Loadable({
    // 要懒加载的组件
    loader: () => import('./Home'),
    // 加载过程
    loading: MyLoadingComponent
});


// myLoadAble(loader, loading)

export default AsyncHome