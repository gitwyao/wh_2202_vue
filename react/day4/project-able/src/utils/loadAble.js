import Loadable from "react-loadable"

const MyLoadingComponent = () => {
    return <div>
        加载中~~~
    </div>
}

function myLoadAble(loader, loading = MyLoadingComponent) {
    return Loadable({
        // 要懒加载的组件
        loader,
        // 加载过程
        loading
    })
}

export default myLoadAble