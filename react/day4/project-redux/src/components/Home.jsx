import React, { Component } from 'react'
import store from '../store'
import axios from 'axios'

export default class Home extends Component {
    // constructor(props) {
    //     super(props)
    //     store.subscribe(() => {
    //         this.setState({})
    //     })
    // }

    add = () => {
        store.dispatch({ type: "ADD_NUM" })
        // this.setState({})
    }
    del = () => {
        store.dispatch({ type: "DEL_NUM" })
        // this.setState({})
    }
    componentDidMount() {
        // 真
        // 假
        store.dispatch((next) => {
            // next  没有经过中间件包装之前的dispatch
            console.log(next)
            axios.get('https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata').then(res => {
                next({ type: "INIT", list: res.data.message })
            })
        })
        // store.dispatch({ type: "INIT" })
    }
    render() {
        console.log(store.getState().list)
        return (
            <div>
                <button onClick={ this.del }>----</button>
                <h2>
                    我是Home -- { store.getState().num }
                </h2>
                <button onClick={ this.add }>+++</button> 
            </div>
        )
    }
}
