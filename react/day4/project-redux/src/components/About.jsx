import React, { Component } from 'react'
import store from '../store'

export default class About extends Component {
    render() {
        return (
            <div>
                <h2>
                    我是About  -- { store.getState().num }
                </h2>
            </div>
        )
    }
}
