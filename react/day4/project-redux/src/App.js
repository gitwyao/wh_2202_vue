import React, { Component } from 'react'
import Home from './components/Home'
import About from './components/About'
import store from './store'
import './App.css'

// Redux(独立的第三方库)  

// function App() {
//   return (
//     <div className="App">
//       <Home/>
//       <About/>
//     </div>
//   )
// }

class App extends Component {
  constructor(props) {
    super(props)
    store.subscribe(() => {
      this.setState({})
    })
  }
  render() {
    return (
      <div className="App">
        <Home />
        <About />
      </div>
    )
  }
}


export default App;
