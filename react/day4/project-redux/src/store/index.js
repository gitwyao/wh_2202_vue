import { createStore, applyMiddleware } from 'redux'
import axios from 'axios'
// import logger from 'redux-logger'
import thunk from 'redux-thunk'

// react-redux

// 创建仓库
// reducer(函数 ==> 逻辑处理)
// initState(初始状态)
// applyMiddleware(应用中间件)
// createStore(reducer, initState, applyMiddleware)  

// reducer(state, action)

const reducer = (state, { type, list }) => {
    // reducer  两种情况会执行
    // 1. 首次
    // 2. 触发dispatch函数会再次执行reducer
    const NewStaet = JSON.parse(JSON.stringify(state))
    switch(type) {
        // case "INIT":
        //     axios.get("https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata").then(res => {
        //         state.list = res.data.message
        //     })
        //     return state
        case "INIT":
            NewStaet.list = list
            return NewStaet
        case "ADD_NUM":
            NewStaet.num += 1
            return NewStaet
        case "DEL_NUM":
            NewStaet.num -= 1
            return NewStaet
        default:
            return state
    }
}

const initState = {
    name: "张三",
    num: 100,
    list: []
}

const store = createStore(reducer, initState, applyMiddleware(thunk))

// store.getState()  读取的是reducer函数的返回值
// store.dispatch()  用于派发任务，根据任务条件来修改数据
// 1. dispatch(actions)  actions必须是一个对象
// 2. dispatch(actions)  actions中必须有一个type属性(string类型)
// 3. store.subscribe 

// console.log(store)

// redux-thunk   把dispatch给修改了   dispatch可以接收函数作为参数
// 超级dispatch


export default store