import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import RootRouter from '../router/RootRouter'


export default class Home extends Component {
    render() {
        const { child } = this.props
        return (
            <div>
                <div>
                    <NavLink to="/home/one">one</NavLink>---
                    <NavLink to="/home/two">two</NavLink>
                </div>
                <RootRouter routes = { child }/>
            </div>
        )
    }
}
