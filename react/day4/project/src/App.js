import { NavLink } from 'react-router-dom'
import RootRouter from './router/RootRouter'
import routes from './router/Routs.config'
import './App.css';

function App() {
  return (
    <div className="App">
      <NavLink to="/home">首页</NavLink>
      <NavLink to="/list">列表</NavLink>
      <NavLink to="/my">我的</NavLink>

      <RootRouter routes = { routes }/>
    </div>
  );
}

export default App;
