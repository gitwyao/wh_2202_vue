import Home from '../pages/Home'
import List from '../pages/List'
import My from '../pages/My'
import About from '../pages/About'
import Login from '../pages/Login'

import One from '../pages/home/One'
import Two from '../pages/home/Two'


export default [
    {
        from: "/",
        to: "/home"
    },
    {
        path: "/home",
        component: Home,
        children: [
            {
                from: "/home",
                to: "/home/one"
            },
            {
                path: "/home/one",
                component: One
            },
            {
                path: "/home/two",
                component: Two,
                auth: true
            }
        ]
    },
    {
        path: "/list",
        component: List
    },
    {
        path: "/my",
        component: My,
        auth: true
    },
    {
        path: "/about",
        component: About
    },
    {
        path: "/login",
        component: Login
    }
]