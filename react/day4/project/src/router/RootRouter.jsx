import React, { Component } from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'

export default class RootRouter extends Component {
    render() {
        const { routes } = this.props
        // 筛选不需要重定向的数据
        const filterRoutes = routes.filter(item => !item.to)
        // 筛选需要重定向的数据
        const redirectRoutes = routes.filter(item => item.to)
        return (
            <Switch>
                {
                    filterRoutes.map((item, index) => {
                        return <Route path={item.path}
                            //   component = { item.component }
                            render={() => {
                                const Com = item.component
                                // 查看是否需要拦截
                                if (item.auth && !sessionStorage.getItem('token')) {
                                    return <Redirect to={{
                                        pathname: "/login",
                                        backUrl: item.path
                                    }} />
                                } else {
                                    // 判断是否有子路由
                                    if (item.children) {
                                        return <Com child={item.children} />
                                    } else {
                                        return <Com />
                                    }
                                }
                            }}
                            key={index} />
                    })
                }

                {
                    redirectRoutes.map((item, index) => {
                        return <Redirect to={item.to} key={index} />
                    })
                }
            </Switch>
        )
    }
}
