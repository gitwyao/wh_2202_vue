import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  layout: {
    "name": "王者荣耀后台管理",
    "logo": "https://img0.baidu.com/it/u=1712165180,2151354116&fm=253&fmt=auto&app=138&f=JPEG?w=626&h=500"
  },
  routes: [
    {
      path: '/', component: '@/pages/index',
      wrappers: [
        '@/wrappers/auth',
      ]
    },
    {
      path: '/ad',
      component: '@/pages/ad',
      name: "广告位管理",
      routes: [
        {
          path: 'list',
          component: '@/pages/ad/List',
          name: "广告位列表"
        }
      ],
      wrappers: [
        '@/wrappers/auth'
      ]
    },
    {
      path: '/item',
      component: '@/pages/item',
      name: "物品管理",
      routes: [
        {
          path: 'list',
          component: '@/pages/item/List',
          name: "物品列表"
        }
      ],
      wrappers: [
        '@/wrappers/auth'
      ]
    },
    {
      path: '/article',
      component: '@/pages/article',
      name: "文章管理",
      routes: [
        {
          path: 'list',
          component: '@/pages/article/List',
          name: "文章列表"
        },
        {
          path: 'create',
          component: '@/pages/article/CreateArticle',
          name: "添加文章"
        }
      ],
      wrappers: [
        '@/wrappers/auth'
      ]
    },
    {
      path: '/hero',
      component: '@/pages/hero',
      name: "英雄管理",
      routes: [
        {
          path: 'list',
          component: '@/pages/hero/List',
          name: "英雄列表"
        },
        {
          path: 'create',
          component: '@/pages/hero/CreateHero',
          name: "添加英雄"
        }
      ],
      wrappers: [
        '@/wrappers/auth'
      ]
    },
    {
      path: "/login",
      component: "@/pages/login/Login",
      headerRender: false,
      footerRender: false,
      menuRender: false
    }
  ],
  dva: {
    // immer: true,
    hmr: true,
  },
  proxy: {
    '/api': {
      'target': 'http://localhost:3333/admin/api',
      'changeOrigin': true,
      'pathRewrite': { '^/api': '' },
    },
  },
  fastRefresh: {},
});
