import axios from '@/request'

export interface IUserInfo {
    username: string,
    password: string
}

function userLogin(params: IUserInfo) {
    return axios.post("/api/login", params)
}

export {
    userLogin
}