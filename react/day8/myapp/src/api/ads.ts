import axios from '@/request'

// 获取广告位列表
function getAdsList() {
    return axios.get("/api/rest/ads")
}

// 删除
function delAdsList(id: string) {
    return axios.delete("/api/rest/ads/" + id)
}

// 添加广告位
function addAdsItem(params: any) {
    return axios.post("/api/rest/ads/", params)
}

export {
    getAdsList,
    delAdsList,
    addAdsItem
}