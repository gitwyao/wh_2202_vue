import axios from '@/request'

export interface IPageInfo {
    pagenum?: number,
    pagesize?: number,
    query?: string
}

interface IItem {
    desc?: string,
    detail?: string,
    icon?: string,
    name?: string,
    star?: number,
    key?: string
}

// 获取物品列表
function getItemList(params?: IPageInfo) {
    return axios.get("/api/rest/items", {
        params
    })
}
// 添加物品
function addItem(params: IItem) {
    return axios.post("/api/rest/items", params)
}
// 删除物品
function delItem(id: string) {
    return axios.delete("/api/rest/items/" + id)
}

// 编辑物品
function editItem(id: string, params: IItem) {
    return axios.put("/api/rest/items/" + id, params)
}

// 获取物品详情
function gettItemDetail(id: string) {
    return axios.get("/api/rest/items/" + id)
}

export {
    getItemList,
    addItem,
    delItem,
    editItem,
    gettItemDetail
}