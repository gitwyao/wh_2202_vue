import axios from '@/request'
import { IPageInfo } from './item'

function getHeroList(params?: IPageInfo) {
    return axios.get("/api/rest/heros", {
        params
    })
}

export {
    getHeroList
}