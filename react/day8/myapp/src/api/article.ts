import axios from '@/request'
import { IPageInfo } from './item'

function getArticleList(params: IPageInfo) {
    return axios.get("/api/rest/articles", {
        params
    })
}

// 获取全部分类
function getCategoriesList() {
    return axios.get("/api/rest/categories")
}

// 添加文章
function addCategoriesItem(params: any) {
    return axios.post("/api/rest/articles", params)
}

// 获取文章详情
function getCategoriesItem(id: string) {
    return axios.get("/api/rest/articles/" + id)
}

// 编辑文章详情
function editCategoriesItem(id: string, params: any) {
    return axios.put("/api/rest/articles/" + id, params)
}

export {
    getArticleList,
    getCategoriesList,
    addCategoriesItem,
    getCategoriesItem,
    editCategoriesItem
}