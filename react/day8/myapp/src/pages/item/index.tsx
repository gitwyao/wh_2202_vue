import { PageHeaderWrapper } from '@ant-design/pro-layout'
import React, { ReactNode } from 'react'
import style from '@/pages/index.less'

type Props = {
    children: ReactNode
}

export default function index({ children }: Props) {
    return (
        <div>
            <PageHeaderWrapper />
            <div className={style.wrapper}>
                {children}
            </div>
        </div>
    )
}