import React, { useState, useEffect } from 'react'
import { Button, Table, Space, Modal } from 'antd'
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import type { ColumnsType } from 'antd/lib/table'
import { getItemList, delItem } from '@/api/item'
import style from '@/pages/index.less'
import { useAddKey } from '@/hooks'
import { message } from 'antd'

import ItemCom from './components/ItemCom'

interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
    description: string;
}

type Props = {}

const { confirm } = Modal;

export default function List({ }: Props) {
    const [list, setList] = useState([])
    const [total, setTotal] = useState(0)
    // 添加弹框
    const [isModalVisible, setIsModalVisible] = useState(false)
    // 编辑弹框
    const [isEditModalVisible, setIsEditModalVisible] = useState(false)
    const [editId, setEditId] = useState<string>("")
    const [pageInfo, setPageInfo] = useState({
        pagenum: 1,
        pagesize: 5
    })

    const columns: ColumnsType<DataType> = [
        {
            title: '序号',
            dataIndex: 'index',
            key: 'index',
            render: (_, __, index) => {
                return index + 1
            },
            width: 70,
            align: 'center'
        },
        { title: '装备名称', dataIndex: 'name', key: 'name', align: 'center' },
        {
            title: '图标',
            dataIndex: 'icon',
            key: 'icon',
            align: 'center',
            render: (icon) => {
                return <img style={{
                    width: 45
                }} src={icon} />
            },
        },
        { title: '星级', dataIndex: 'star', key: 'star', align: 'center' },
        {
            title: '操作',
            dataIndex: '',
            key: 'x',
            align: 'center',
            render: (row) => <Space>
                <Button
                    icon={<EditOutlined />}
                    onClick={() => {
                        setIsEditModalVisible(true)
                        setEditId(row["key"])
                        // setEditItemInfo(row)
                    }}
                    type="primary">编辑</Button>
                <Button icon={<DeleteOutlined />}
                    type="primary"
                    onClick={() => {
                        confirm({
                            title: '确定要删除嘛?',
                            icon: <ExclamationCircleOutlined />,
                            onOk() {
                                delItem(row._id)
                                init()
                            },
                            onCancel() {
                                message.info("取消删除")
                            }
                        })
                    }}
                    danger>删除</Button>
            </Space>,
        },
    ];


    const showModal = () => {
        setIsModalVisible(true);
    };

    // 添加弹框取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };

    useEffect(() => {
        init()
    }, [pageInfo])

    // 初始化
    const init = () => {
        getItemList(pageInfo).then((res: any) => {
            const data = useAddKey(res.data)
            setTotal(res.total)
            setList(data)
        })
    }

    const changePageInfo = (page: any) => {
        setPageInfo({
            pagenum: page.current,
            pagesize: page.pageSize
        })
    }

    return (
        <div className={style.wrapperBox}>
            <Button type="primary" onClick={showModal}>+ 添加装备</Button>
            <ItemCom title="新增" isEdit = { false} vis = { isModalVisible } setVis = { setIsModalVisible } init={init}/>
            <ItemCom title="编辑" isEdit = { true } editId={editId} vis = { isEditModalVisible } setVis = { setIsEditModalVisible } init={init}/>
            <div className={style.mt20}>
                <Table
                    columns={columns}
                    dataSource={list}
                    bordered
                    pagination={{
                        total,
                        pageSizeOptions: [5, 8, 10, 15],
                        defaultPageSize: 5,
                        pageSize: pageInfo.pagesize,
                        current: pageInfo.pagenum
                    }}
                    onChange={changePageInfo}
                    expandable={{
                        // 展开填充的内容
                        expandedRowRender: (record: any) => <p style={{ margin: 0, textAlign: 'center' }}>
                            {record.detail}
                        </p>,
                        // 不需要展开
                        // rowExpandable: record => record.name !== 'Not Expandable',
                    }}
                />
            </div>
        </div>
    )
}