import React, { useState } from 'react'
import { Modal, Input, Upload, Rate, Button } from 'antd'
import style from '@/pages/index.less'
import { addItem } from '@/api/item'
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface'
import { getBase64, beforeUpload } from '@/utils/upload'
import type { UploadChangeParam } from 'antd/es/upload'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'

type Props = {
    init: Function,
    vis: boolean,
    setVis: Function
}

const desc = ['1', '2', '3'];

export default function AddItem({ init, vis, setVis }: Props) {
    // const [isModalVisible, setIsModalVisible] = useState(false)
    // 上传
    const [loading, setLoading] = useState(false);
    // 添加数据
    const [itemInfo, setItemInfo] = useState<{ name?: string, icon?: string, star?: number, desc?: any, detail?: string }>({
        name: "",
        icon: "",
        star: 0,
        desc: "",
        detail: ""
    })

    // 添加弹框确定
    const handleOk = () => {
        setVis(false)
        addItem(itemInfo)
        setItemInfo({
            star: 0
        })
        init()
    };

    // 添加弹框取消
    const handleCancel = () => {
        setVis(false);
    };

    // 新增上传
    const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
        if (info.file.status === 'uploading') {
            setLoading(true)
            return
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj as RcFile, icon => {
                setLoading(false)
                setItemInfo({
                    ...itemInfo,
                    icon
                })
            });
        }
    };

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );
    return (
        <div>
            <Modal title="添加装备" visible={vis} onOk={handleOk} onCancel={handleCancel}>
                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品名称
                    </span>
                    <div>
                        <Input value={itemInfo.name} onChange={(ev) => {
                            setItemInfo({
                                ...itemInfo,
                                name: ev.target.value
                            })
                        }} />
                    </div>
                </div>

                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品图标
                    </span>
                    <div>
                        <Upload
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            // 添加token字段
                            // headers={{
                            //     Authorization: "123"
                            // }}
                            action="/api/upload/item"
                            beforeUpload={beforeUpload}
                            onChange={handleChange}
                        >
                            {itemInfo.icon ? <img src={itemInfo.icon} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                        </Upload>
                    </div>
                </div>

                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        星级
                    </span>
                    <div>
                        <span>
                            <Rate count={3} onChange={(star) => {
                                setItemInfo({
                                    ...itemInfo,
                                    star
                                })
                            }} value={itemInfo.star} />
                            <span className="ant-rate-text">
                                {itemInfo.star ? desc[itemInfo.star - 1] : '0'}
                            </span>
                        </span>
                    </div>
                </div>
                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品简介
                    </span>
                    <div>
                        <Input value={itemInfo.desc} onChange={(ev) => {
                            setItemInfo({
                                ...itemInfo,
                                desc: ev.target.value
                            })
                        }} />
                    </div>
                </div>
                <div className={style.dis}>
                    <span className={style.right}>
                        物品详情
                    </span>
                    <div>
                        <Input value={itemInfo.detail} onChange={(ev) => {
                            setItemInfo({
                                ...itemInfo,
                                detail: ev.target.value
                            })
                        }} />
                    </div>
                </div>
            </Modal>
        </div>
    )
}