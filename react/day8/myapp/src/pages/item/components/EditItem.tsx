import React, { useState, useEffect } from 'react'
import { Modal, Input, Upload, Rate } from 'antd'
import style from '@/pages/index.less'
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface'
import { getBase64, beforeUpload } from '@/utils/upload'
import type { UploadChangeParam } from 'antd/es/upload'
import { editItem, gettItemDetail } from '@/api/item'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'

type Props = {
    init: Function,
    vis: boolean,
    setVis: Function,
    editId: string
}

const desc = ['1', '2', '3'];

export default function EditItem({ init, vis, setVis, editId }: Props) {
    // 编辑数据
    const [editItemInfo, setEditItemInfo] = useState({
        name: "",
        icon: "",
        star: 0,
        desc: "",
        detail: "",
        key: ""
    })

    // 上传
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        gettItemDetail(editId).then((res: any) => {
            setEditItemInfo(res)
        })
    }, [editId])

    // 编辑弹框确定
    const handleEditOk = () => {
        setVis(false)
        editItem(editItemInfo["key"], editItemInfo)
        init()
    };

    // 编辑弹框取消
    const handleEditCancel = () => {
        setVis(false);
    };

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );

    // 编辑上传
    const handleEdidChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
        if (info.file.status === 'uploading') {
            setLoading(true)
            return
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj as RcFile, icon => {
                setLoading(false)
                setEditItemInfo({
                    ...editItemInfo,
                    icon
                })
            });
        }
    };
    return (
        <div>
            <Modal title="编辑装备" visible={vis} onOk={handleEditOk} onCancel={handleEditCancel}>
                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品名称
                    </span>
                    <div>
                        <Input value={editItemInfo.name} onChange={(ev) => {
                            setEditItemInfo({
                                ...editItemInfo,
                                name: ev.target.value
                            })
                        }} />
                    </div>
                </div>

                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品图标
                    </span>
                    <div>
                        <Upload
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            // 添加token字段
                            // headers={{
                            //     Authorization: "123"
                            // }}
                            action="/api/upload/item"
                            beforeUpload={beforeUpload}
                            onChange={handleEdidChange}
                        >
                            {editItemInfo.icon ? <img src={editItemInfo.icon} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                        </Upload>
                    </div>
                </div>

                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        星级
                    </span>
                    <div>
                        <span>
                            <Rate count={3} onChange={(star) => {
                                setEditItemInfo({
                                    ...editItemInfo,
                                    star
                                })
                            }} value={editItemInfo.star} />
                            <span className="ant-rate-text">
                                {editItemInfo.star ? desc[editItemInfo.star - 1] : '0'}
                            </span>
                        </span>
                    </div>
                </div>
                <div className={[style.dis, style.mb20].join(" ")}>
                    <span className={style.right}>
                        物品简介
                    </span>
                    <div>
                        <Input value={editItemInfo.desc} onChange={(ev) => {
                            setEditItemInfo({
                                ...editItemInfo,
                                desc: ev.target.value
                            })
                        }} />
                    </div>
                </div>
                <div className={style.dis}>
                    <span className={style.right}>
                        物品详情
                    </span>
                    <div>
                        <Input value={editItemInfo.detail} onChange={(ev) => {
                            setEditItemInfo({
                                ...editItemInfo,
                                detail: ev.target.value
                            })
                        }} />
                    </div>
                </div>
            </Modal>
        </div>
    )
}