import React, { useEffect, useState } from 'react'
import { Col, Card, Tag, Input, Button, Select } from 'antd'
import adsStyle from '@/components/ads/ads.less'
import { getHeroList } from '@/api/hero'
import { connect } from 'umi'

type Props = {
    index: number,
    createHero: any,
    types: string,
    changeRelationItem: Function
}

const { TextArea } = Input
const { Option } = Select

function MyCard({ index, createHero, types, changeRelationItem }: Props) {
    const [list, setList] = useState([])
    useEffect(() => {
        getHeroList().then((res: any) => {
            setList(res)
        })
    }, [])

    const handleChange = (value: string) => {
        changeRelationItem(types, index, "hero", value)
    };
    return (
        <Col span={12}>
            <div className={adsStyle.card}>
                <Card title={<Tag color="blue">
                    英雄 {index + 1}
                </Tag>} bordered={false}>
                    <div className={adsStyle.flex}>
                        <span>
                            英雄名称:
                        </span>
                        <div>
                            <Select style={{ width: 120 }} onChange={handleChange}>
                                {
                                    list.map((item) => {
                                        return <Option key={item["_id"]} value={item["_id"]}>
                                            {item["name"]}
                                        </Option>
                                    })
                                }
                            </Select>
                        </div>
                    </div>
                    <div className={adsStyle.flex}>
                        <span>
                            描述:
                        </span>
                        <div>
                            <TextArea
                                value={createHero[types].length > 0 ? createHero[types][index].desc : ''}
                                onChange={(ev) => {
                                    changeRelationItem(types, index, "desc", ev.target.value)
                                }} />
                        </div>
                    </div>

                    <div className={adsStyle.flex}>
                        <span>
                            <Button type="primary" danger>删除</Button>
                        </span>
                    </div>
                </Card>
            </div>
        </Col>
    )
}

const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        changeRelationItem(types: string, index: number, key: string, value: string) {
            dispatch({ type: "createHero/changeRelationItem", types, index, key, value })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCard)