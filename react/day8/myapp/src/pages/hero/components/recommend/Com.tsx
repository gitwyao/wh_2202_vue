import React, { useEffect, useState } from 'react'
import { Select, Input } from 'antd'
import style from '@/pages/index.less'
import { getItemList } from '@/api/item'
import { connect } from 'umi'

type Props = {
    title: string,
    createHero: any,
    types: string,
    setItem: Function
}

const { Option } = Select
const { TextArea } = Input

function Com({ title, createHero, types, setItem }: Props) {
    const [list, setList] = useState([])
    useEffect(() => {
        getItemList().then((res: any) => {
            setList(res)
        })
    }, [])

    const handleChange = (value: string[]) => {
        setItem("equipment", value, types)
    };
    return (
        <div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    {title}出装
                </span>
                <div>
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: '100%' }}
                        onChange={handleChange}
                    >
                        {
                            list.map((item: any) => {
                                return <Option key={item["_id"]}>
                                    {item.name}
                                </Option>
                            })
                        }
                    </Select>
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    {title}技巧
                </span>
                <div>
                    <TextArea value={createHero[types].tips} onChange={(ev) => {
                        setItem("tips", ev.target.value, types)
                    }} />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setItem(key: string, value: string, shipin: string) {
            dispatch({ type: "createHero/changeHeroItem", key, value, shipin })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Com)