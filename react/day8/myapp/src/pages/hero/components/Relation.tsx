import React from 'react'
import { Tabs, Button, Row } from 'antd'
import { connect } from 'umi'
import Card from './relation/Card'


type Props = {
  partners: any,
  restrained: any,
  restraint: any,
  addRelation: Function
}

const { TabPane } = Tabs;
const onChange = (key: string) => {
  console.log(key);
};

// desc
// hero

function Relation({
  partners,
  restrained,
  restraint,
  addRelation }: Props) {
  return (
    <div>
      <Tabs defaultActiveKey="1" onChange={onChange}>
        <TabPane tab="最佳搭档" key="1">
          <Button type="primary" onClick={() => {
            addRelation("partners")
          }}>+ 添加英雄</Button>
          <Row gutter={16}>
            {
              partners.map((item: any, index: number) => {
                return <Card types="partners" index={index} key={index} />
              })
            }
          </Row>

        </TabPane>
        <TabPane tab="被谁克制" key="2">
          <Button type="primary" onClick={() => {
            addRelation("restrained")
          }}>+ 添加英雄</Button>
          <Row gutter={16}>
            {
              restrained.map((item: any, index: number) => {
                return <Card types="restrained" index={index} key={index} />
              })
            }
          </Row>

        </TabPane>
        <TabPane tab="克制谁" key="3">
          <Button type="primary" onClick={() => {
            addRelation("restraint")
          }}>+ 添加英雄</Button>
          <Row gutter={16}>
            {
              restraint.map((item: any, index: number) => {
                return <Card types="restraint" index={index} key={index} />
              })
            }
          </Row>
        </TabPane>
      </Tabs>
    </div>
  )
}

const mapStateToProps = (state: any) => {
  const { createHero } = state
  const { partners, restrained, restraint } = createHero
  return {
    partners,
    restrained,
    restraint
  }
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    setItem(key: string, value: string, shipin: string) {
      dispatch({ type: "createHero/changeHeroItem", key, value, shipin })
    },
    addRelation(types: string) {
      dispatch({ type: "createHero/addRelation", types })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Relation)