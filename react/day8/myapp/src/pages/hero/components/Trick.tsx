import React from 'react'
import { Input } from 'antd'
import { connect } from 'umi'
import style from '@/pages/index.less'


const { TextArea } = Input

type Props = {
  createHero: any,
  setItem: Function
}

function Trick({ createHero, setItem }: Props) {
  return (
    <div>
      <div className={[style.dis, style.mb20].join(" ")}>
        <span className={[style.right, style.ml20].join(" ")}>
          使用技巧
        </span>
        <div>
          <TextArea value={createHero.usageTips} onChange={(ev) => {
            setItem("usageTips", ev.target.value)
          }} />
        </div>
      </div>
      <div className={[style.dis, style.mb20].join(" ")}>
        <span className={[style.right, style.ml20].join(" ")}>
          对抗技巧
        </span>
        <div>
          <TextArea value={createHero.battleTips} onChange={(ev) => {
            setItem("battleTips", ev.target.value)
          }} />
        </div>
      </div>
      <div className={[style.dis, style.mb20].join(" ")}>
        <span className={[style.right, style.ml20].join(" ")}>
          团战思路
        </span>
        <div>
          <TextArea value={createHero.teamTips} onChange={(ev) => {
            setItem("teamTips", ev.target.value)
          }} />
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => {
  const { createHero } = state
  return {
    createHero
  }
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    setItem(key: string, value: string, shipin: string) {
      dispatch({ type: "createHero/changeHeroItem", key, value, shipin })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Trick)