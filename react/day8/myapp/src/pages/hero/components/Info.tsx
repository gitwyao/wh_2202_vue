import React from 'react'
import style from '@/pages/index.less'
import { Select } from 'antd'
import HeroInput from './info/HeroInput'
import HeroSelect from './info/HeroSelect'
import HeroRate from './info/HeroRate'
import HeroUpload from './info/HeroUpload'

type Props = {}
const { Option } = Select;

export default function Info({ }: Props) {
  return (
    <div>
      <HeroInput heroTitle="名称" field="name" />
      <HeroInput heroTitle="称号" field="title" />

      <HeroSelect/>

      <HeroRate scores={true} field="difficulty" title="难度" />
      <HeroRate scores={true} field="skill" title="技能" />
      <HeroRate scores={true} field="attack" title="攻击" />
      <HeroRate scores={true} field="survive" title="生存" />

      <HeroUpload field="avatar" title="英雄头像" />
      <HeroUpload field="banner" title="背景图片" />
      <HeroUpload field="photo" title="图文介绍" />
    </div>
  )
}

// avatar: "http://192.168.59.147:3333/uploads/heros/41f49c13c14359f29fd465cb01b7daab"
// banner: "http://192.168.59.147:3333/uploads/heros/f21b070ce370819884dd76eb6be4d8d7"
// cate: ["5e1426cb0eac252758fdffd6", "5e1426d70eac252758fdffd7"]
// downWind: {equipment: []}
// name: "111"
// partners: []
// photo: "http://192.168.59.147:3333/uploads/heros/fe58b0a507110fce7c7a41cfec4b6001"
// restrained: []
// restraint: []
// scores: {difficulty: 5, skill: 6, attack: 7, survive: 8}
// shipin: {}
// skills: []
// skins: []
// title: "22"
// upWind: {equipment: []}