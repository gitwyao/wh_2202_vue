import React, { useEffect, useState } from 'react'
import style from '@/pages/index.less'
import { Select } from 'antd'
import { connect } from 'umi'
import { getCategoriesList } from '@/api/article'

type Props = {
    setItem: Function,
    createHero: any
}

const { Option } = Select;

function HeroSelect({
    setItem,
    createHero
}: Props) {
    const [list, setList] = useState([])
    useEffect(() => {
        getCategoriesList().then((res: any) => {
            setList(res[0].children)
        })
    }, [])

    const handleChange = (value: string[]) => {
        setItem("cate", value)
    };
    return (
        <div className={[style.dis, style.mb20].join(" ")}>
            <span className={[style.ml20, style.right].join(" ")}>
                英雄定位:
            </span>
            <div>
                <Select
                    mode="multiple"
                    allowClear
                    style={{ width: '100%' }}
                    onChange={handleChange}
                    value={createHero.cate}
                >
                    {
                        list.map((item: any) => {
                            return <Option key={item["_id"]}>
                                { item.name }
                            </Option>
                        })
                    }
                </Select>
            </div>
        </div>
    )
}
const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setItem(key: string, value: string) {
            dispatch({ type: "createHero/changeHeroItem", key, value })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeroSelect)