import React from 'react'
import style from '@/pages/index.less'
import { Input } from 'antd'
import { connect } from 'umi'

type Props = {
    heroTitle: string,
    field: string,
    createHero: any,
    setItem: Function
}

function HeroInput({ 
    heroTitle,
    field,
    createHero,
    setItem
}: Props) {
    return (
        <div className={[style.dis, style.mb20].join(" ")}>
            <span className={[style.ml20, style.right].join(" ")}>
                英雄{heroTitle}:
            </span>
            <div>
                <Input value={ createHero[field] } onChange={(ev) => {
                    setItem(field, ev.target.value)
                }}/>
            </div>
        </div>
    )
}
const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setItem(key: string, value: string) {
            dispatch({ type: "createHero/changeHeroItem", key, value })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeroInput)