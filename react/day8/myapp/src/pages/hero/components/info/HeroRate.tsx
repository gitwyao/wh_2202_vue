import React, { useState } from 'react'
import style from '@/pages/index.less'
import { Rate } from 'antd'
import { connect } from 'umi'

type Props = {
    title: string,
    scores: boolean,
    field: string,
    createHero: any,
    setItem: Function
}
const desc = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

function HeroRate({ 
    title,
    createHero,
    scores,
    field,
    setItem
}: Props) {
    return (
        <div className={[style.dis, style.mb20].join(" ")}>
            <span className={[style.ml20, style.right].join(" ")}>
                { title }:
            </span>
            <div>
                <span>
                    <Rate count={10} tooltips={desc} onChange={(value) => {
                        setItem(field, value, scores)
                    }} value={createHero.scores[field]} />
                    {createHero.scores[field] ? <span className="ant-rate-text">{desc[createHero.scores[field] - 1]}</span> : ''}
                </span>
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setItem(key: string, value: string, scores: boolean) {
            dispatch({ type: "createHero/changeHeroItem", key, value, scores })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeroRate)