import React, { useState } from 'react';
import { connect } from 'umi'
import { Input, Upload, message } from 'antd'
import style from '@/pages/index.less'
import '@/pages/index.less'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import type { UploadChangeParam } from 'antd/es/upload';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';

const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result as string));
    reader.readAsDataURL(img);
};

const beforeUpload = (file: RcFile) => {
    const isMp4 = file.type === 'video/mp4'
    if (!isMp4) {
        message.error('You can only upload Mp4 file!');
    }
    const isLt200M = file.size / 1024 / 1024 < 200;
    if (!isLt200M) {
        message.error('Image must smaller than 200MB!');
    }
    return isMp4 && isLt200M;
};


type Props = {
    shipin: any,
    setItem: Function
}

function Video({ shipin, setItem }: Props) {
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState<string>();
    const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj as RcFile, url => {
                setLoading(false);
                setImageUrl(url);
                setItem("video", url, "shipin")
            });
        }
    };

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );
    return (
        <div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    标题
                </span>
                <div>
                    <Input value={shipin.title} onChange={(ev) => {
                        setItem("title", ev.target.value, 'shipin')
                    }} />
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    视频
                </span>
                <div>
                    <Upload
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="/api/upload/introduction"
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                    >
                        {imageUrl ? <video controls style={{ width: "100%" }} src={imageUrl}></video> : uploadButton}
                    </Upload>
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    投稿
                </span>
                <div>
                    <Input value={shipin.submit} onChange={(ev) => {
                        setItem("submit", ev.target.value, 'shipin')
                    }} />
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    粉丝
                </span>
                <div>
                    <Input value={shipin.fan} onChange={(ev) => {
                        setItem("fan", ev.target.value, 'shipin')
                    }} />
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={[style.right, style.ml20].join(" ")}>
                    观看次数
                </span>
                <div>
                    <Input value={shipin.view} onChange={(ev) => {
                        setItem("view", ev.target.value, 'shipin')
                    }} />
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = (state: any) => {
    const { createHero } = state
    const { shipin } = createHero
    return {
        shipin
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setItem(key: string, value: string, shipin: string) {
            dispatch({ type: "createHero/changeHeroItem", key, value, shipin })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Video)