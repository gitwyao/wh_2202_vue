import React from 'react'
import Com from './recommend/Com'

type Props = {}

export default function Recommend({ }: Props) {
    return (
        <div>
            <Com title="顺风" types="downWind"/>
            <Com title="逆风" types="upWind"/>
        </div>
    )
}