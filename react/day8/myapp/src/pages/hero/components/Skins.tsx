import React, { useState } from 'react'
import { Button, Row, Col, Tag, Card, Input, Upload } from 'antd'
import style from '@/pages/index.less'
import adsStyle from '@/components/ads/ads.less'
import { connect } from 'umi'
import { getBase64, beforeUpload } from '@/utils/upload'
import type { UploadChangeParam } from 'antd/es/upload'
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

type Props = {
  createHero: any,
  setItem: Function,
  addSkins: Function,
  delSkins: Function,
  changeArrItem: Function
}
// skins [{}]
// img
// name

function Skins({ createHero, addSkins, delSkins, changeArrItem }: Props) {
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState<string>();
  const handleChange = (info: UploadChangeParam<UploadFile>, index: number) => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj as RcFile, url => {
        setLoading(false)
        changeArrItem(index, "img", url)
        // setImageUrl(url);
      });
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );
  return (
    <div>
      <Button onClick={() => {
        addSkins()
      }} className={style.mb20} type="primary">+ 添加皮肤</Button>
      <div>
        <Row gutter={16}>
          {
            createHero.skins.map((item: any, index: number) => {
              return <Col key={index} span={12}>
                <div className={adsStyle.card}>
                  <Card title={<Tag color="blue">
                    皮肤{index + 1}
                  </Tag>} bordered={false}>
                    <div className={adsStyle.flex}>
                      <span>
                        皮肤名称:
                      </span>
                      <div>
                        <Input value={item.name} onChange={(ev) => {
                          changeArrItem(index, "name", ev.target.value)
                        }} />
                      </div>
                    </div>
                    <div className={adsStyle.flex}>
                      <span>
                        图片:
                      </span>
                      <div>
                        <Upload
                          listType="picture-card"
                          className="avatar-uploader"
                          showUploadList={false}
                          action="/api/upload/skin"
                          beforeUpload={beforeUpload}
                          onChange={(info) => {
                            handleChange(info, index)
                          }}
                        >
                          {/*  */}
                         {createHero.skins[index].img ? <img src={createHero.skins[index].img} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                        </Upload>
                      </div>
                    </div>

                    <div className={adsStyle.flex}>
                      <span>
                        <Button onClick={() => {
                          delSkins(index)
                        }} type="primary" danger>删除</Button>
                      </span>
                    </div>
                  </Card>

                </div>
              </Col>
            })
          }
        </Row>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => {
  const { createHero } = state
  return {
    createHero
  }
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    addSkins() {
      dispatch({ type: "createHero/addSkins" })
    },
    delSkins(index: number) {
      dispatch({ type: "createHero/delSkins", index })
    },
    setItem(key: string, value: string) {
      dispatch({ type: "createHero/changeHeroItem", key, value })
    },
    changeArrItem(index: number, types: string, value: string) {
      dispatch({ type: "createHero/changeArrItem", index, types, value })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Skins)