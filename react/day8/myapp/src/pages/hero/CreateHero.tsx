import { Tabs, Button } from 'antd';
import type { SizeType } from 'antd/lib/config-provider/SizeContext';
import React, { useState } from 'react';
import style from '@/pages/index.less'
import Info from './components/Info'
import Relation from './components/Relation'
import Skins from './components/Skins'
import Video from './components/Video'
import Recommend from './components/Recommend'
import Trick from './components/Trick'
import Skills from './components/Skills'
import { connect } from 'umi'

type Props = {
    createHero: any
}

const { TabPane } = Tabs;

function CreateHero({ createHero }: Props) {
    const [size, setSize] = useState<SizeType>('small');
    const [active, setActive] = useState("1")

    const onTabClick = (key: string) => {
        // setSize(e.target.value);
        setActive(key)
    };

    return (
        <div className={style.wrapperBox}>
            <Tabs activeKey={active} defaultActiveKey="1" type="card" onTabClick={onTabClick} size={size}>
                <TabPane tab="基本信息" key="1">
                    <Info/>
                </TabPane>
                <TabPane tab="英雄皮肤" key="2">
                    <Skins/>
                </TabPane>
                <TabPane tab="介绍视频" key="3">
                    <Video/>
                </TabPane>
                <TabPane tab="出装推荐" key="4">
                    <Recommend/>
                </TabPane>
                <TabPane tab="使用技巧" key="5">
                    <Trick/>
                </TabPane>
                <TabPane tab="技能管理" key="6">
                    <Skills/>
                </TabPane>
                <TabPane tab="英雄关系" key="7">
                   <Relation/>
                </TabPane>
            </Tabs>

            <Button onClick={() => {
                console.log(createHero)
            }} type="primary">保存</Button>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { createHero } = state
    return {
        createHero
    }
}

export default connect(mapStateToProps)(CreateHero)