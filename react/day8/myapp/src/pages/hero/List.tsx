import React, { useState, useEffect } from 'react'
import style from '@/pages/index.less'
import { AudioOutlined } from '@ant-design/icons'
import { Input, Button, Table, Space, Row, Col } from 'antd'
import type { ColumnsType } from 'antd/lib/table'
import { connect } from 'umi'

type Props = {
    list: [],
    getList: Function,
    total: number
}
const { Search } = Input
const suffix = (
    <AudioOutlined
        style={{
            fontSize: 16,
            color: '#1890ff'
        }}
    />
)

interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
    description: string;
    skills: Array<any>
}

const columns: ColumnsType<DataType> = [
    {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        width: 80,
        align: 'center',
        render: (_, __, index) => index + 1
    },
    {
        title: '英雄名称',
        dataIndex: 'name',
        key: 'name',
        align: 'center',
        width: 180
    },
    { title: '英雄称号', dataIndex: 'title', key: 'title', align: 'center', width: 180 },
    {
        title: '所属分类',
        dataIndex: 'cate',
        key: 'cate',
        align: 'center',
        width: 180,
        render: (row) => <Space>
            {
                row.map((item: any, index: number) => {
                    return <span key={item["_id"]}>
                        {item.name}
                        {index === row.length - 1 ? '' : '/'}
                    </span>
                })
            }
        </Space>
    },
    {
        title: '头像',
        dataIndex: 'avatar',
        key: 'avatar',
        align: 'center',
        width: 180,
        render: (icon) => {
            return <img style={{
                width: 45
            }} src={icon} />
        },
    },
    {
        title: '操作',
        dataIndex: '',
        key: 'x',
        align: 'center',
        render: (row, __, index) => <Space key={index}>
            <Button type="primary">编辑</Button>
            <Button type="primary" danger>删除</Button>
        </Space>,
    },
];


function List({ list, getList, total }: Props) {
    const [pageInfo, setPageInfo] = useState({
        pagenum: 1,
        pagesize: 5
    })

    useEffect(() => {
        getList(pageInfo)
    }, [pageInfo])

    const onSearch = (query: string) => {
        getList({
            ...pageInfo,
            query
        })
    }

    const changePageInfo = (page: any) => {
        setPageInfo({
            pagenum: page.current,
            pagesize: page.pageSize
        })
    }

    return (
        <div className={style.wrapperBox}>
            <Search className={style.mb20} onSearch={onSearch} style={{ width: 200 }} />
            <Button className={style.ml20} type="primary">+ 添加英雄</Button>
            <div>
                <Table
                    columns={columns}
                    bordered
                    pagination={{
                        total,
                        pageSizeOptions: [5, 8, 10, 15],
                        defaultPageSize: 5
                    }}
                    expandable={{
                        expandedRowRender: record => {
                            return <Row className={ style.center }>
                                {
                                    record.skills.map(item => {
                                        return <Col span={6}>
                                            <img style={{
                                                width: 50
                                            }} src={ item.icon } alt="" />
                                            <p style={{
                                                lineHeight: "24px"
                                            }}>
                                            { item.name }
                                            </p>
                                        </Col>
                                    })
                                }
                            </Row>
                        },
                        rowExpandable: record => record.skills.length !== 0,
                    }}
                    onChange={changePageInfo}
                    dataSource={list}
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { hero } = state
    const { list, total } = hero
    return {
        list,
        total
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        getList(pageInfo: any) {
            dispatch({ type: "hero/getList", pageInfo })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
