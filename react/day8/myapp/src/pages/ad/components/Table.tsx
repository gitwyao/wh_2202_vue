import React, { useEffect, useState } from 'react'
import { Space, Table, Button, message, Popconfirm } from 'antd'
import type { ColumnsType } from 'antd/lib/table'
import { getAdsList, delAdsList } from '@/api/ads'
import { useAddKey, useTableList } from '@/hooks'
import style from '../list.less'

type Props = {}

interface DataType {
    key: string;
    name: string;
    _id: string;
}

const cancel = () => {
    message.error('取消删除')
};

export default function MyTable({ }: Props) {
    const { init, list } = useTableList()

    // 删除按钮
    const confirm = (id: string) => {
        delAdsList(id).then(res => { })
        init()
    };

    const columns: ColumnsType<DataType> = [
        {
            title: '广告位名称',
            dataIndex: 'name',
            key: 'name',
            align: 'center',
            render: text => <a>{text}</a>
        },
        {
            title: '操作',
            key: 'action',
            align: 'center',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary">编辑</Button>
                    <Popconfirm
                        title="确定要删除么？"
                        onConfirm={() => {
                            console.log(record)
                            confirm(record._id)
                        }}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type="primary" danger>删除</Button>
                    </Popconfirm>
                </Space>
            )
        }
    ]

    useEffect(() => {
        init()
    }, [])
    return (
        <div className={ style.mt20 }>
            <Table bordered columns={columns} dataSource={list} />
        </div>
    )
}