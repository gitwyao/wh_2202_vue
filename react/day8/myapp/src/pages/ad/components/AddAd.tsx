import React, { useState, useEffect } from 'react'
import AdsCard from '@/components/ads/AdsCard'
import { connect } from 'umi'
import { addAdsItem } from '@/api/ads'
import { Space, Table, Button, message, Popconfirm, Modal, Input, Row } from 'antd'
import type { ColumnsType } from 'antd/lib/table'
import { delAdsList } from '@/api/ads'
import { useTableList } from '@/hooks'
import style2 from '../list.less'
import style from '../list.less'

type Props = {
    addList: Function,
    bannerList: [],
    reset: Function
}


interface DataType {
    key: string;
    name: string;
    _id: string;
}

const cancel = () => {
    message.error('取消删除')
};


function AddAd({ addList, bannerList, reset }: Props) {
    const [visible, setVisible] = useState(false)
    const [val, setVal] = useState("")
    const { init, list } = useTableList()

    // 删除按钮
    const confirm = (id: string) => {
        delAdsList(id).then(res => { })
        init()
    };

    const columns: ColumnsType<DataType> = [
        {
            title: '广告位名称',
            dataIndex: 'name',
            key: 'name',
            align: 'center',
            render: text => <a>{text}</a>
        },
        {
            title: '操作',
            key: 'action',
            align: 'center',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary">编辑</Button>
                    <Popconfirm
                        title="确定要删除么？"
                        onConfirm={() => {
                            console.log(record)
                            confirm(record._id)
                        }}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type="primary" danger>删除</Button>
                    </Popconfirm>
                </Space>
            )
        }
    ]

    useEffect(() => {
        init()
    }, [])
    // 添加广告位
    const success = () => {
        setVisible(false)
        addAdsItem({
            name: val,
            items: list
        }).then(res => {
            init()
        })
    }

    return (
        <div>
            <>
                <Button type="primary" onClick={() => setVisible(true)}>
                    + 添加广告位
                </Button>
                <Modal
                    title="添加广告位"
                    visible={visible}
                    onOk={success}
                    onCancel={() => {
                        setVisible(false)
                        reset()
                    }}
                    width={1000}
                >
                    <div className={style.dis}>
                        <span>
                            广告位名称:
                        </span>
                        <div>
                            <Input onChange={(ev) => {
                                setVal(ev.target.value)
                            }} />
                        </div>
                    </div>

                    <Button className={style.mt20} onClick={() => {
                        addList()
                    }} type="primary">
                        + 添加广告位信息
                    </Button>

                    <Row gutter={16}>
                        {
                            bannerList.map((item, index) => {
                                return <AdsCard index={index} key={index} />
                            })
                        }
                    </Row>
                </Modal>

                {/* 表格 */}
                <div className={style2.mt20}>
                    <Table bordered columns={columns} dataSource={list} />
                </div>
            </>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { ads } = state
    const { list } = ads
    return {
        bannerList: list
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        addList() {
            dispatch({ type: "ads/setList" })
        },
        reset() {
            dispatch({ type: "ads/resetList" })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddAd)