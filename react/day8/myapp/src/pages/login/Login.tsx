import React from 'react'
import style from './login.less'
import { Card, Button, Form, Input } from 'antd'
import { userLogin, IUserInfo } from '@/api/user'
import Cookie from 'js-cookie'
import { history } from 'umi'

type Props = {}

export default function Login({ }: Props) {
    const onFinish = (values: IUserInfo) => {
        userLogin(values).then((res: any) => {
            Cookie.set('token', res.token)
            history.push('/')
        })
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className={style.login}>
            <Card className={ style.card } style={{ width: 450 }}>
                <h2 className={ style.h2 }>管理员登录</h2>
                <Form
                    name="basic"
                    wrapperCol={{ span: 24 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: '请输入账号!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: '请输入密码!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item>
                        <Button style={{
                            width: "100%"
                        }} type="primary" htmlType="submit">
                            登录
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}