import React, { useEffect, useState } from 'react'
import style from '@/pages/index.less'
import { Table, Button, Space } from 'antd';
import type { ColumnsType } from 'antd/lib/table'
import { connect, history } from 'umi'

type Props = {
    list: Array<any>,
    getList: Function,
    total: number
}

interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
    description: string;
}

const columns: ColumnsType<DataType> = [
    {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        width: 80,
        align: 'center',
        render: (_, __, index) => index + 1
    },
    {
        title: '所属分类',
        dataIndex: 'cate',
        key: 'cate',
        align: 'center',
        width: 180,
        render: (row) => <Space>
            {
                row.map((item: any, index: number) => {
                    return <span key={item["_id"]}>
                        {item.name}
                        {index === row.length - 1 ? '' : '/'}
                    </span>
                })
            }
        </Space>,
    },
    { title: '标题', dataIndex: 'title', key: 'title', align: 'center', },
    {
        title: '操作',
        dataIndex: '',
        key: 'x',
        align: 'center',
        render: (row, __, index) => <Space key={index}>
            <Button onClick={() => {
                history.push({
                    pathname: '/article/create',
                    state: {
                        id: row["_id"]
                    }
                })
            }} type="primary">编辑</Button>
            <Button type="primary" danger>删除</Button>
        </Space>,
    },
];

const List = ({ list, getList, total }: Props) => {
    const [pageInfo, setPageInfo] = useState({
        pagenum: 1,
        pagesize: 5
    })

    useEffect(() => {
        getList(pageInfo)
    }, [pageInfo])

    const changePageInfo = (page: any) => {
        setPageInfo({
            pagenum: page.current,
            pagesize: page.pageSize
        })
    }

    return (
        <div className={style.wrapperBox}>
            <Button type="primary"
                    onClick={() => {
                        history.push('/article/create')
                    }}
                    className={ style.mb20 }>+ 添加文章</Button>
            <Table
                columns={columns}
                bordered
                pagination={{
                    total,
                    pageSizeOptions: [5, 8, 10, 15],
                    defaultPageSize: 5,
                    pageSize: pageInfo.pagesize,
                    current: pageInfo.pagenum
                }}
                onChange={changePageInfo}
                dataSource={list}
            />
        </div>
    )
}

const mapStateToProps = (state: any) => {
    const { article } = state
    const { list, total } = article
    return {
        list,
        total
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        getList(pageInfo: any) {
            dispatch({ type: "article/getList", pageInfo })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)