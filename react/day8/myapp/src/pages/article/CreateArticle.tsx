import React, { useEffect, useState } from 'react'
import style from '@/pages/index.less'
import { Select, Input, Button } from 'antd'
import { 
    getCategoriesList, 
    addCategoriesItem, 
    getCategoriesItem,
    editCategoriesItem
 } from '@/api/article'
import '@wangeditor/editor/dist/css/style.css' // 引入 css
import { Editor, Toolbar } from '@wangeditor/editor-for-react'
import { IDomEditor, IEditorConfig } from '@wangeditor/editor'
import { history } from 'umi'

type Props = {}

const { Option } = Select;


export default function CraeteArticle({ }: Props) {
    const [list, setList] = useState<any>([])
    const [subInfo, setSubInfo] = useState<{
        cate: string[],
        title: string
    }>({
        cate: [],
        title: ""
    })
    // 富文本编辑器
    const [editor, setEditor] = useState<IDomEditor | null>(null) // 存储 editor 实例
    const [html, setHtml] = useState('') // 编辑器内容

    const handleChange = (cate: string[]) => {
        console.log('---', cate)
        setSubInfo({
            ...subInfo,
            cate
        })
    };
    let id = ""
    if (history.location.state) {
        id = history.location.state.id
    }

    useEffect(() => {
        getCategoriesList().then((res: any) => {
            setList(res)
        })
        
        if (id !== "") {
            getCategoriesItem(id).then((res: any) => {
                console.log(res)
                setSubInfo({
                    cate: res.cate,
                    title: res.title
                })
                setHtml(res.content)
            })
            console.log('编辑页面')
        } else {
            console.log('添加页面')
        }
    }, [])

    const toolbarConfig = {}
    const editorConfig: Partial<IEditorConfig> = {
        placeholder: '请输入内容...',
    }

    const sub = () => {
        if (id !== "") {
            console.log(html)
            editCategoriesItem(id, {
                ...subInfo,
                content: html
            })
        } else {
            addCategoriesItem({
                ...subInfo,
                content: html
            })
        }
        history.push('/article/list')
    }

    // 及时销毁 editor ，重要！
    useEffect(() => {
        return () => {
            if (editor == null) return
            editor.destroy()
            setEditor(null)
        }
    }, [editor])
    return (
        <div className={style.wrapperBox}>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={style.right}>
                    所属分类:
                </span>
                <div>
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: 250 }}
                        placeholder="Please select"
                        onChange={handleChange}
                        value={subInfo.cate}
                    >
                        {
                            list[1] ? list[1].children.map((item: any) => {
                                return <Option key={item["_id"]}>
                                    {item["name"]}
                                </Option>
                            }) : ''
                        }
                    </Select>
                </div>
            </div>
            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={style.right}>标题:</span>
                <div>
                    <Input value={subInfo.title} onChange={(ev) => {
                        setSubInfo({
                            ...subInfo,
                            title: ev.target.value
                        })
                    }} />
                </div>
            </div>

            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={style.right}>内容:</span>
                <div>
                    <>
                        <div style={{ border: '1px solid #ccc', zIndex: 100 }}>
                            <Toolbar
                                editor={editor}
                                defaultConfig={toolbarConfig}
                                mode="default"
                                style={{ borderBottom: '1px solid #ccc' }}
                            />
                            <Editor
                                defaultConfig={editorConfig}
                                value={html}
                                onCreated={setEditor}
                                onChange={(editor) => {
                                    setHtml(editor.getHtml())
                                }}
                                mode="default"
                                style={{ height: '300px', 'overflowY': 'hidden' }}
                            />
                        </div>
                    </>
                </div>
            </div>

            <div className={[style.dis, style.mb20].join(" ")}>
                <span className={style.right}></span>
                <div>
                    <Button onClick={() => {
                       sub()
                    }} type="primary">保存</Button>
                </div>
            </div>
        </div>
    )
}