import { useState } from 'react'
import { getAdsList } from '@/api/ads'
import Cookie from 'js-cookie'

function useAuth() {
    // let isLogin = sessionStorage.getItem('token')? true: false
    let isLogin = Cookie.get('token') ? true : false
    return {
        isLogin
    }
}

function useAddKey(array: any) {
    array.forEach((item: any) => item.key = item._id)
    return array
}

function useTableList() {
    const [list, setList] = useState([])
    const init = () => {
        getAdsList().then((res: any) => {
            res = useAddKey(res)
            setList(res)
        })
    }
    return {
        list,
        init
    }
}

export {
    useAuth,
    useAddKey,
    useTableList
}