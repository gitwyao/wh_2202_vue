export default [
    {
        url: "/api/login",
        method: "post"
    },
    {
        url: "/api/rest/ads",
        method: "delete"
    },
    {
        url: "/api/rest/ads",
        method: "post"
    },
    {
        url: "/api/rest/items",
        method: "post"
    },
    {
        url: "/api/rest/items",
        method: "delete"
    },
    {
        url: "/api/rest/items",
        method: "put"
    },
    {
        url: "/api/rest/articles",
        method: "post"
    },
    {
        url: "/api/rest/articles",
        method: "put"
    }
]