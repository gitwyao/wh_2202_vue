import React from 'react';
import { ReactNode } from 'react';
import { Redirect } from 'umi'
import { useAuth } from '../../hooks'

interface IProps {
    children: ReactNode
}

export default (props: IProps) => {
    const { isLogin } = useAuth()
    if (isLogin) {
        return <div>
            { props.children }
        </div>
    } else {
        return <Redirect to="/login"/>
    }
}