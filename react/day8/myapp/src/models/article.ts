import { Effect, ImmerReducer, Reducer, Subscription } from 'umi'
import { getArticleList } from '@/api/article'
import { useAddKey } from '@/hooks'

export interface AbsModelState {
    list: Array<any>,
    total: number
}

export interface AbsModelType {
    state: AbsModelState;
    effects: {
        getList: Effect;
    };
    reducers: {
        setList: Reducer<AbsModelState>
    };
    subscriptions?: { setup: Subscription };
}

const AbsModel: AbsModelType = {
    state: {
        list: [],
        total: 0
    },
    effects: {
        *getList({ pageInfo }, { call, put }) {
            const data = yield getArticleList(pageInfo)
            yield put({ type: "setList", list: useAddKey(data.data), total: data.total })
        }
    },
    reducers: {
        setList(state, action) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list = action.list
            NewState.total = action.total
            return NewState
        }
    }
};

export default AbsModel;
