import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

interface IArrayItem {
    img: string,
    url: string
}

export interface AbsModelState {
    list: Array<IArrayItem>
}

export interface AbsModelType {
    state: AbsModelState;
    effects: {
        query: Effect;
    };
    reducers: {
        setList: Reducer<AbsModelState>
        resetList: Reducer<AbsModelState>
        delItem: Reducer<AbsModelState>
        setItemUrl: Reducer<AbsModelState>
        setItemSrc: Reducer<AbsModelState>
    };
    subscriptions?: { setup: Subscription };
}

const AbsModel: AbsModelType = {
    state: {
        list: []
    },
    effects: {
        *query({ payload }, { call, put }) { },
    },
    reducers: {
        setList(state, action) {
            let NewState = JSON.parse(JSON.stringify(state))
            if (NewState) {
                let obj = {
                    img: "",
                    url: "",
                    id: NewState.list.length
                }
                NewState.list.push(obj)
                console.log(NewState.list)
            }
            return NewState
        },
        resetList(state) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list = []
            return NewState
        },
        delItem(state, { index }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list.splice(index, 1)
            console.log(NewState.list)
            return NewState
        },
        setItemUrl(state, { index, val }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list[index].url = val
            return NewState
        },
        setItemSrc(state, { index, src }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list[index].img = src
            return NewState
        }
    }
};

export default AbsModel;
