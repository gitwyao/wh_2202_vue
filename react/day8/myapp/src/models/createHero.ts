import { Effect, ImmerReducer, Reducer, Subscription } from 'umi'

export interface CreateHeroModelState {
    name: string,
    title: string,
    cate: Array<string>,
    scores: any,
    avatar: string,
    // 背景
    banner: string,
    // 图文
    photo: string,
    // 皮肤
    skins: Array<any>,
    // 介绍视频
    shipin: any,
    // 使用技巧
    usageTips: string,
    // 对抗技巧
    battleTips: string,
    // 团战思路
    teamTips: string,
    // 顺风
    downWind: any,
    // 逆风
    upWind: any,
    // 最佳搭档
    partners: any,
    // 被谁克制
    restrained: any,
    // 克制谁
    restraint: any
}

export interface CreateHeroModelType {
    state: CreateHeroModelState;
    effects: {

    };
    reducers: {
        changeHeroItem: Reducer<CreateHeroModelState>
        addSkins: Reducer<CreateHeroModelState>
        delSkins: Reducer<CreateHeroModelState>
        changeArrItem: Reducer<CreateHeroModelState>
        addRelation: Reducer<CreateHeroModelState>
        changeRelationItem: Reducer<CreateHeroModelState>
    };
    subscriptions?: { setup: Subscription };
}

const CreateHeroModel: CreateHeroModelType = {
    state: {
        name: "",
        title: "",
        cate: [],
        scores: {
            difficulty: null,
            skill: null,
            attack: null,
            survive: null
        },
        avatar: "",
        banner: "",
        photo: "",
        skins: [],
        shipin: {
            title: "",
            video: "",
            submit: "",
            fan: "",
            view: ""
        },
        usageTips: "",
        battleTips: "",
        teamTips: "",
        downWind: {
            equipment: [],
            tips: ""
        },
        upWind: {
            equipment: [],
            tips: ""
        },
        partners: [],
        restrained: [],
        restraint: []
    },
    effects: {

    },
    reducers: {
        changeHeroItem(state, { key, value, scores, shipin }) {
            let NewState = JSON.parse(JSON.stringify(state))
            if (scores) {
                NewState.scores[key] = value
            } else if (shipin) {
                // 介绍视频
                NewState[shipin][key] = value
            } else {
                NewState[key] = value
            }
            return NewState
        },
        // 修改数组里的内容
        changeArrItem(state, { index, types, value }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.skins[index][types] = value
            return NewState
        },
        addSkins(state) {
            let NewState = JSON.parse(JSON.stringify(state))
            const obj = {
                name: "",
                img: ""
            }
            NewState.skins.push(obj)
            return NewState
        },
        delSkins(state, { index }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.skins.splice(index, 1)
            return NewState
        },
        addRelation(state, { types }) {
            let NewState = JSON.parse(JSON.stringify(state))
            const obj = {
                desc: "",
                hero: ""
            }
            NewState[types].push(obj)
            return NewState
        },
        // 修改数组里的内容
        changeRelationItem(state, { types, index, key, value }) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState[types][index][key] = value
            return NewState
        },
    }
};

export default CreateHeroModel;
