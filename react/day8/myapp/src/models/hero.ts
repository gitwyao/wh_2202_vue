import { Effect, ImmerReducer, Reducer, Subscription } from 'umi'
import { getHeroList } from '@/api/hero'
import { useAddKey } from '@/hooks'

export interface HeroModelState {
    list: Array<any>,
    total: number
}

export interface HeroModelType {
    state: HeroModelState;
    effects: {
        getList: Effect;
    };
    reducers: {
        setList: Reducer<HeroModelState>
    };
    subscriptions?: { setup: Subscription };
}

const HeroModel: HeroModelType = {
    state: {
        list: [],
        total: 0
    },
    effects: {
        *getList({ pageInfo }, { call, put }) {
           const data = yield getHeroList(pageInfo)
           yield put({ type: "setList", list: useAddKey(data.data), total: data.total })
        }
    },
    reducers: {
        setList(state, action) {
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list = action.list
            NewState.total = action.total
            return NewState
        }
    }
};

export default HeroModel;
