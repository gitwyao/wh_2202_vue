import type { RcFile } from 'antd/es/upload/interface';
import { message } from 'antd'

const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result as string));
    reader.readAsDataURL(img);
};

// 上传之前的限制条件
const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('请上传jpg/png格式!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('图片应小于2MB!');
    }
    return isJpgOrPng && isLt2M;
};

export {
    getBase64,
    beforeUpload
}

