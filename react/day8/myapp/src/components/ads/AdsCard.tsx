import React, { useState, useEffect } from 'react'
import { Col, Card, Tag, Input, Button, message, Upload, Row } from 'antd'
import style from './ads.less'
// 上传
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import type { UploadChangeParam } from 'antd/es/upload';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';
import { getBase64, beforeUpload } from '@/utils/upload'
import { connect } from 'umi'

type Props = {
    index: number,
    delItem: Function,
    setItemUrl: Function,
    setItemSrc: Function,
    list: []
}

function AdsCard({ index, delItem, list, setItemUrl, setItemSrc }: Props) {
    // 上传的状态
    const [loading, setLoading] = useState(false);
    // 图片的链接
    const [imageUrl, setImageUrl] = useState<string>();

    const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
        // 上传中
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        // 上传完毕
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj as RcFile, url => {
                setLoading(false);
                setImageUrl(url);
                setItemSrc(url, index)
            });
        }
    };
    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );
    return (
        <Col span={12}>
            <div className={style.card}>
                <Card title={<Tag color="blue">
                    广告位{index + 1}
                </Tag>} bordered={false}>
                    <div className={style.flex}>
                        <span>
                            图片:
                        </span>
                        <div>
                            <Upload
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                action="/api/upload/ad"
                                beforeUpload={beforeUpload}
                                onChange={handleChange}
                            >
                                {imageUrl ? <img src={ list.length > 0?list[index]["img"]: '' } alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                            </Upload>
                        </div>
                    </div>
                    <div className={style.flex}>
                        <span>
                            跳转连接:
                        </span>
                        <div>
                            <Input value = { list.length > 0?list[index]["url"]: '' } onChange={(ev) => {
                                setItemUrl(ev.target.value, index)
                            }}/>
                        </div>
                    </div>
                    <div className={style.flex}>
                        <span>
                            <Button onClick={() => {
                                console.log(index)
                                delItem(index)
                            }} type="primary" danger>删除</Button>
                        </span>
                    </div>
                </Card>
            </div>
        </Col>
    )
}

const mapStateToProps = (state: any) => {
    const { ads } = state
    const { list } = ads
    return {
        list
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        delItem(index: number) {
            dispatch({ type: "ads/delItem", index })
        },
        setItemUrl(val: string, index: number) {
            dispatch({ type: "ads/setItemUrl", val, index })
        },
        setItemSrc(src: string, index: number) {
            dispatch({ type: "ads/setItemSrc", src, index })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AdsCard)