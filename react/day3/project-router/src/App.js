import { Link, Route, NavLink, Redirect, Switch } from 'react-router-dom'
import Home from './pages/Home';
import Home2 from './pages/Home2';
import About from './pages/About';
import Login from './pages/Login';
import './App.css';

// react-router-dom5 步骤
// 1. 根组件外包裹  HashRouter, BrowserRouter
// 2. <Link to>123</Link>
// 3. <Route />
// 4. <Switch></Switch>   只能包裹 <Route/>

function App() {
  return (
    <div className="App">
      {/* <Link to="/home">首页</Link>---
      <Link to="/about">测试</Link>---
      <Link to="/home2">首页2</Link> */}

      {/* activeClassName  区分类名 */}
      {/* activeStyle  区分样式*/}

      {/* <NavLink activeStyle={{
        color: 'orange'
      }} to="/home">首页</NavLink>---
      <NavLink activeStyle={{
        color: 'orange'
      }} to="/about">测试</NavLink>---
      <NavLink activeStyle={{
        color: 'orange'
      }} to="/home2">首页2</NavLink> */}

      {/* Route  三种情况 */}
      {/* 1. component  (必须是组件名称) 会自动给当前组件添加路由信息 */}
      {/* <Route path="/home" component = { Home }/>
      <Route path="/about" component = { About }/> */}
      {/* 2. render (必须是函数) 不会给当前组件携带路由信息  自定义传递属性*/}
      {/* <Route path="/home" render={(props) => {
          // return <Home  {...props}/>
          return <div>
            <h3>面包屑</h3>
            <Home/>
          </div>
      }}/>
      <Route path="/home2"  render={() => {
          return <Home2/>
      }}/>
      <Route path="/about"  render={() => {
          return <About/>
      }}/> */}
      {/* 3. children  自定义传递属性 */}
      {/* <Route  path="/home">
          <Home/>
      </Route>

      <Route  path="/about">
          <About/>
      </Route>

      <Route path="/home2">
          <Home2/>
      </Route>

      <Route path="/login">
          <Login/>
      </Route>

      <Redirect from="/" to="/home2"/> */}


      {/* 包容性匹配 */}

      <NavLink exact activeStyle={{
        color: 'orange'
      }} to="/home">首页1</NavLink>---
      <NavLink exact activeStyle={{
        color: 'orange'
      }} to="/about">测试</NavLink>---
      <NavLink exact activeStyle={{
        color: 'orange'
      }} to="/home/abc">首页2</NavLink>

      {/* exact  开启精准匹配 */}
      {/* <Route exact path="/home">
        <Home />
      </Route>

      <Route exact path="/about">
        <About />
      </Route>

      <Route exact path="/home/abc">
        <Home2 />
      </Route> */}

      {/* Switch  有一个满足条件--终止 */}
      <Switch>
        <Route path="/home/abc">
          <Home2 />
        </Route>

        <Route path="/home">
          <Home />
        </Route>

        <Route path="/about">
          <About />
        </Route>
      </Switch>

    </div>
  );
}

export default App;
