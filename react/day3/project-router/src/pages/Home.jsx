import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { isAuth } from '../high'
// withRouter  高阶组件 ==>  给当前组件添加路由信息

class Home extends Component {
    render() {
        console.log(this.props)
        return (
            <div>
                <button onClick={() => {
                    this.props.history.push('/about')
                }}>点击跳转</button>
            </div>
        )
    }
}

export default withRouter(isAuth(Home))
