import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'

function addRoutes(Com) {
    return class NewCom extends Component {
        render() {
            return <Route component={ Com }/>
        }
    }
}


function isAuth(Com) {
    return class NewCom extends Component {
        render() {
            // 判断是否登录
            if (sessionStorage.getItem('token')) {
                return <Com/>
            } else {
                return <Redirect to="/login"/>
            }
        }
    }
}


export {
    addRoutes,
    isAuth
}