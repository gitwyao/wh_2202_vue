import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        console.log(this.props)
        // ['/', '/home', '/my']
        // ['/', '/my']
        // push  会记录上一次地址
        // replace   不会记录上一次地址
        return (
            <div>
                首页 -- <button onClick={() => {
                    this.props.history.push('/my')
                }}>点我</button>
            </div>
        )
    }
}
