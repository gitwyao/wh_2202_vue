import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class List extends Component {
    state = {
        list: [
            {
                id: 1,
                title: "商品1"
            },
            {
                id: 2,
                title: "商品2"
            },
            {
                id: 3,
                title: "商品3"
            }
        ]
    }

    pushDetail = (id) => {
        // react路由传参
        //  Vue   query, params
        //  React
        // this.props.history.push("/detail?id=" + id)   获取不到
        // 1. 随意写, 临时传参
        // this.props.history.push({
        //     pathname: "/detail",
        //     bbb: {
        //         id
        //     }
        // })

        // 2. state  刷新不会丢失
        // this.props.history.push({
        //     pathname: "/detail",
        //     state: {
        //         id
        //     }
        // })

        // 3 /detail/3
        console.log(id)
        // tabbar
        this.props.history.push('/detail/' + id)
    }

    render() {
        const { list } = this.state
        return (
            <div>
                {
                    list.map((item, index) => {
                        // 声明式
                        // return <Link to={{
                        //     pathname: '/detail',
                        //     state: {
                        //         id: item.id
                        //     }
                        // }} key={index}>
                        //     { item.title }
                        // </Link>

                        return <Link to={'/detail/' + item.id } key={index}>
                            { item.title }
                        </Link>


                        // 编程式
                        // return <div onClick = { this.pushDetail.bind(this, item.id) } key={index}>
                        //     { item.title }
                        // </div>

                        // return <div onClick={() => {
                        //     this.pushDetail(item.id)
                        // }} key={index}>
                        //     { item.title }
                        // </div>
                    })
                }
            </div>
        )
    }
}
