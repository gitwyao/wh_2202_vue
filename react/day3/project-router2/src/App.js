import { Route, NavLink } from 'react-router-dom'
import Home from './pages/Home'
import List from './pages/List'
import My from './pages/My'
import Detail from './pages/Detail'
import './App.css';


function App() {
  return (
    <div className="App">
      <main>
        <Route component={Home} path="/home" />
        <Route component={List} path="/list" />
        <Route component={My} path="/my" />
        <Route component={Detail} path="/detail"/>
        {/* <Route component={Detail} path="/detail/:id"/> */}
      </main>

      <footer>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/list">列表</NavLink>
        <NavLink to="/my">我的</NavLink>
      </footer>
    </div>
  );
}

export default App;
