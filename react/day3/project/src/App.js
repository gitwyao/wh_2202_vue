import Com from './components/Com';
import PropsTypes from './components/PropsTypes';
import State from './components/State';
import Ant from './components/Ant';
import MyTable from './components2/MyTable';
import './App.css';


function App() {
  return (
    <div className="App">
      {/* <Com></Com> */}

      {/* 子节点为单个 */}
      {/* <Com>
        <div>11111</div>
      </Com> */}

      {/* <Com>
        <>22222</>
        <>33333</>
      </Com> */}


      {/* <PropsTypes/> */}


      {/* <State/> */}


      {/* <Ant/> */}


      <MyTable/>
    </div>
  );
}

export default App;


// 虚拟Dom  +  diff(对比)  
// {
//     type: "div",
//     props: {},
//     children: [
//         {
//             type: "span",
//             key: '0',
//             children: [
//                 {
//                     type: "span",
//                     key: '0',
//                     children: [
//                         {
//                             type: 'span',
//                             key: '3',
//                             children: [345]
//                         }
//                     ]
//                 }
//             ]
//         }
//     ]
// }
