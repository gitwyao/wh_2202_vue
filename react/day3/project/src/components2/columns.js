import { Divider, Tag, Button } from 'antd'

const columns = [
    {
        // 表格
        title: '名称',
        // 控制data数据中的key 的名称
        dataIndex: 'name',
        // 唯一值
        key: 'name',
        // 渲染视图
        render(text, row, index){
            // text  当前(dataIndex)的内容
            // row   当前匹配的数据所有的内容
            // console.log(text, row, index)
            return <a>
                {text}
            </a>
        }
    },
    {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: '地址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '标签',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <span>
                {/*  ['nice', 'developer'] */}
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </span>
        ),
    },
    {
        title: '操作',
        key: 'action',
        render: (text, record) => (
            <span>
                <Button type="primary">编辑</Button>
                <Divider type="vertical" />
                <Button onClick={() => {
                    console.log(this)
                }} type="danger">删除</Button>
            </span>
        )
    }
]

export default columns