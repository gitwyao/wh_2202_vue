import React, { Component } from 'react'

export default class State extends Component {
    state = {
        ipt: "123",
        pwd: "abc"
    }

    setAtr = (atr, ev) => {
        this.setState({
            [atr]: ev.target.value
        })
    }

    render() {
        // 受控组件
        const { ipt, pwd } = this.state
        return (
            <div>
                <div>
                    {/* 非受控组件   ref */}
                    <input ref="ipt" type="text" />
                </div>

                <div>
                    姓名：<input value={ipt} onChange={(ev) => {
                       this.setAtr('ipt', ev)
                    }} type="text" />
                </div>
                <div>
                    账号：<input value={pwd} onChange={(ev) => {
                        this.setAtr('pwd', ev)
                    }} type="text" />
                </div>
                <div>
                    <button onClick={() => {
                        console.log(ipt, pwd)
                    }}>登录</button>
                </div>

                {/* <div>
                    账号：<input value={ipt} onChange={(ev) => {
                        this.setState({
                            ipt: ev.target.value
                        })
                    }} type="text" />
                </div> */}
            </div>
        )
    }
}
