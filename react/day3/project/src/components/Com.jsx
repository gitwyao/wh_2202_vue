import React, { Component, Children } from 'react'

console.log(Children)
// Children.count   计算子元素的数量
// Children.only   查看子元素是否是唯一的

export default class Com extends Component {
    render() {
        console.log(Children.only(this.props.children))
       
        return (
            <div>
                Com -- 
                {/* { 
                    this.props.children.map((item, index) => {
                        return item + '.00'
                    })
                } */}

                {
                    Children.map(this.props.children, (item) => {
                        return <div>
                            { item }.00
                        </div>
                    })
                }
            </div>
        )
    }
}
