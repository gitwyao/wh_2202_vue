import React, { Component } from 'react'
import PropType from 'prop-types'

export default class PropsTypes extends Component {
    // 静态属性
    // static propTypes = {
    //     str: PropType.string.isRequired
    // }

    // 静态属性  默认值
    static defaultProps = {
        str: "我是默认值--static"
    }
    render() {
        return (
            <div>
                PropsTypes -- { this.props.str }
            </div>
        )
    }
}

// PropsTypes.propTypes = {
//     str: PropType.number
// }

// 默认值
// PropsTypes.defaultProps = {
//     str: "我是默认值"
// }
