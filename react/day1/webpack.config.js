const HtmlWebpackPlugin = require('html-webpack-plugin'); // 通过 npm 安装

module.exports = {
    // entry   .js   .json
    entry: "./src/index.js",
    // output
    mode: "development",
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ template: './public/index.html' })
    ],
    // webpack-cli   webpack-dev-server   webpack
    // 配置前端服务
    devServer: {
        port: 3000,
        open: true
    }
}