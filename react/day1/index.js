import { createElement } from 'react'
import { render } from 'react-dom'
// const { createElement } = require('react')
// const { render } = require('react-dom')
// createElement 创建react元素
// oDiv  react元素  (虚拟Dom)

const oH3 = createElement('h3', {}, "我是h标签")

const oDiv = createElement('div', {
    id: 'oDiv',
    className: 'box'
}, [oH3, 222])



// render(react元素, 挂载点, callback)
render(oDiv, document.getElementById('root'), () => {
    console.log('挂载成功')
})


// webpack  0C

console.log(oDiv)
