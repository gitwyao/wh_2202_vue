import React, { createElement, Component } from 'react';
import { render } from 'react-dom';
import './index.css';

// npm i babel-cli -g (确保能够使用bable命令)
// npm i babel-preset-react -S  (为了识别react语法)

// const oDiv = <div id='oDiv' className='box'>
//     <h3>我是h标签</h3>
//     <span>22222--4444---5555 ---- 666</span>
//     <div>
//         <span>---333333444 -- { 1 + 1}</span>
//     </div>
// </div>


let str = "我是字符串";
let num = 888;

// 特殊的
// 不会展示到页面
let und = undefined;
let nulls = null;
let flag = true;
// 数组会自动展开
let arr = [1, 2, 3, 4, 5];
// 对象不能作为react的子元素   除了react元素以外
let obj = {
    name: "张三"
};

let arr2 = [111, 222, [333, 444, [555]]];
let arr3 = [{
    age: 18
}, {
    age: 28
}];

// const oDiv = <div id='oDiv' className='box'>
//     <p>
//         字符串: { str }
//     </p>
//     <p>
//         数字: { num }
//     </p>
//     <p>
//         布尔值: { flag }
//     </p>
//     <p>
//         数组: { arr }
//     </p>
//     {/* <p>
//         对象: { obj }
//     </p> */}
//     <p>
//         数组2: { arr2 }
//     </p>
//     {/* <p>
//         数组3: { arr3 }
//     </p> */}
//     <p>
//         und: { und }
//     </p>
//     <p>
//         null: { nulls }
//     </p>
// </div>


// v-for
// let list = [1, 2, 3, 4, 5]
// const oDiv = <div id='oDiv' className='box'>
//     <ul>
//         {/* [<li>1</li>, <li>2</li>, <li>3</li>, <li>4</li>, <li>5</li>] */}
//         {
//             list.map((item, index) => {
//                 return <li key={index}>
//                     { item }
//                 </li>
//             })
//         }
//     </ul>
// </div>


// v-if 
// let isShow = '3'
// let h1 = isShow==="1"? <h1>我是标签</h1>: isShow==="2"?  <h2>我是标签</h2>: '小标签'

// const oDiv = <div id='oDiv' className='box'>
//     { h1 }
// </div>


// v-show
// let isShow = false
// const oDiv = <div id='oDiv' className='box'>
//     <h1 className = { isShow? 'block': 'none'} >我是标签</h1>
// </div>

// v-bind


// 组件   16.8以前
// 1. 视图组件(函数式组件)    没有内部状态     没有生命周期

// function App() {
//     let count = 8888
//     console.log('app执行')
//     return <div>
//         <h3>我是函数式组件---{ count }</h3>
//         <button onClick={() => {
//             count = 9999
//             render(<App/>, document.getElementById('root'))
//         }}>点我更新</button>
//     </div>
// }

// function Com() {
//     return <div>
//         组件1
//     </div>
// }
// function Com2() {
//     return <div>
//         组件2
//     </div>
// }

// 2. 类组件    定义自己的内部状态
// class App extends Component {
//     constructor(props) {
//         super(props)
//         // 定义内部状态
//         this.state = {
//             count: 999,
//             info: {
//                 name: "张三",
//                 age: 18
//             },
//             arr: [1, 2, 3, 4, 5]
//         }
//     }
//     // 渲染视图
//     render() {
//         console.log('render函数执行')
//         // 解构赋值
//         const { count, info, arr } = this.state
//         return <div>
//             {/* 我是类组件 -- { count } */}

//             {/* 修改对象 */}
//             {/* <p>
//                 姓名：{ info.name }
//             </p>
//             <p>
//                 年龄：{ info.age }
//             </p> */}

//             {/* 修改数组 */}
//             {arr}
//             <button onClick={() => {
//                 // this.state.count = 888
//                 // console.log(this.state.count)
//                 // render(<App/>, document.getElementById('root'))

//                 // 在react中修改数据this.setState(state, callback)

//                 // this.setState 时而异步(使用合成事件)    时而同步(使用原生事件)
//                 // this.setState({
//                 //     count: 333
//                 // }, () => {
//                 //     console.log(this.state.count)
//                 //     console.log('我确定数据修改了')
//                 // })

//                 // this.setState修改引用类型数据
//                 // let info = this.state.info
//                 // info.name = "李四"
//                 // this.setState({
//                 //     info
//                 // })

//                 // 修改数组
//                 // let arr = JSON.parse(JSON.stringify(this.state.arr))
//                 // arr[2] = 333
//                 // this.setState({
//                 //     arr: arr
//                 // })


//             }}>修改</button>
//         </div>
//     }
// }


// 合成事件
// class App extends Component {
//     init(num, ev) {
//         console.log('我是一个函数', num, ev)
//     }
//     render() {
//         return <div>
//             <h3>我是类组件</h3>

//             <button onClick={ this.init }>点我</button>

//             {/* <button onClick={ (ev) => {
//                 this.init(1111111, ev)
//             } }>点我</button> */}
//         </div>
//     }
// }


// Tab切换
class Tab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            tabs: ["Tab1", "Tab2", "Tab3"]
        };
    }
    changeCount(count) {
        this.setState({
            count
        });
    }
    render() {
        const { tabs, count } = this.state;
        return React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                null,
                tabs.map((item, index) => {
                    return React.createElement(
                        'span',
                        { className: count === index ? 'active' : '',
                            onClick: () => {
                                this.changeCount(index);
                            },
                            key: index },
                        item,
                        ' --'
                    );
                })
            ),
            React.createElement(
                'div',
                null,
                tabs[count]
            )
        );
    }
}

render(React.createElement(Tab, null), document.getElementById('root'));

// render(<App />, document.getElementById('root'), () => {
//     // console.log('挂载成功')
// })

// render(oDiv, document.getElementById('root'), () => {
//     console.log('挂载成功')
// })
