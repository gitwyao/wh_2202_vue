import React from 'react'
import { Select } from 'antd'
import { useOptions } from '../hooks'
const { Option } = Select

const handleChange = (value) => {
    console.log(`selected ${value}`);
};

export default function MySelect({ title, api, keys, id }) {
    const list = useOptions(api)
    return (
        <div>
            <h4>请选择{title}类型:</h4>
            <Select
                style={{
                    width: 120,
                }}
                onChange={handleChange}
            >
                {
                    list.map(item => {
                        return <Option key = { item[id] } value={ item[id] }>
                            { item[keys] }
                        </Option>
                    })
                }
            </Select>
        </div>
    )
}
