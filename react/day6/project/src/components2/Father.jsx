import React from 'react'
import Son from './Son'

export default function Father() {
    return (
        <div>
            <h3>我是父组件</h3>
            <Son/>
        </div>
    )
}
