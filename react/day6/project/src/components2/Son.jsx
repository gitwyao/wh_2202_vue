import React, { useContext } from 'react'
import { Context } from './GrandFather'

export default function Son() {
    const data = useContext(Context)
    return (
        <div>
            <h5>我是子组件 -- { data }</h5>
        </div>
    )
}
