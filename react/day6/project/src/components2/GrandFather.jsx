import React, { createContext, useState } from 'react'
import Father from './Father'

export const Context = createContext()

export default function GrandFather() {
    const [count, setCount] = useState(8888)
    return (
        <div>
            <h1>我是最大的组件--{ count }</h1>
            <Context.Provider value={ count }>
                <Father/> 
            </Context.Provider>
        </div>
    )
}
