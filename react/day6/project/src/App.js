import { useState } from 'react'
import Com from './components/Com';
import Effect from './components/Effect';
import Destroy from './components/Destroy';

import GrandFather from './components2/GrandFather';
import Reducer from './components3/Reducer';

import One from './components5/One';
import Two from './components5/Two';

import Hook from './components6/Hook';
import Hook2 from './components6/Hook2'

import Select from './components7/Select';
import { Button } from 'antd'
import './App.css';

function App() {
  // const [count, setCount] = useState(333)
  // const [flag, setFlag] = useState(true)
  function sub() {
    let obj = {
      "one": '1',
      "two": '2'
    }
    console.log(obj)
  }

  return (
    <div className="App">
      {/* <Com/> */}
      {/* 
      <Effect count = { count }/>
      <h2>
        { count }
      </h2>
      <button onClick={() => {
        setCount(count + 1)
      }}>+1</button> */}

      {/* {
        flag? <Destroy/>: ''
      }
      
      <button onClick={() => {
        setFlag(false)
      }}>销毁</button> */}



      {/* <GrandFather/> */}

      {/* <Reducer/> */}
{/* 
      <One/>
      <Two/> */}


      {/* <Hook/>
      <Hook2/> */}

      <Select id="exam_id" keys="exam_name" api="/exam/examType" title="考试"/>
      <Select id="subject_id" keys="subject_text" api="/exam/subject" title="课程"/>
      <Select id="questions_type_id" keys="questions_type_text" api="/exam/getQuestionsType" title="题目"/>
      
      <Button onClick={() => {
        sub()
      }}>提交</Button>
    </div>
  );
}

// {"msg":"考试类型获取成功","code":1,"data":[{"exam_id":"8sc5d7-7p5f9e-cb2zii-ahe5i","exam_name":"周考1"},{"exam_id":"jpg8y9-zbzt7o-jpvuhf-fwnjvr","exam_name":"周考2"},{"exam_id":"ukmp9b-radddj-ogwdr-nw3ane","exam_name":"周考3"},{"exam_id":"wbxm4-jf8q6k-lvt2ca-ze96mg","exam_name":"月考"}]}

// {"msg":"所有的课程获取成功","code":1,"data":[{"subject_id":"fqtktr-1lq5u","subject_text":"javaScript上"},{"subject_id":"wl5yk-38c0g","subject_text":"javaScript下"},{"subject_id":"8tl7os-r49tld","subject_text":"模块化开发"},{"subject_id":"1ux00o6-2xbj5i","subject_text":"移动端开发"},{"subject_id":"4pu32-vs796l","subject_text":"node基础"},{"subject_id":"1psw2b-cy7o07","subject_text":"组件化开发(vue)"},{"subject_id":"fyu3ln-azjkie","subject_text":"渐进式开发(react)"},{"subject_id":"94sjh6-lnlxe","subject_text":"项目实战"},{"subject_id":"k1gvd4-8lrx8f","subject_text":"javaScript高级"},{"subject_id":"u3ix15-dd6md","subject_text":"node高级"}]}

// {"msg":"数据获取成功","code":1,"data":[{"questions_type_id":"774318-730z8m","questions_type_text":"简答题","questions_type_sort":1},{"questions_type_id":"br9d6s-wh46i","questions_type_text":"代码阅读题","questions_type_sort":2},{"questions_type_id":"fwf0t-wla1q","questions_type_text":"代码补全","questions_type_sort":3},{"questions_type_id":"cll4tv-kt4ppt","questions_type_text":"修改bug","questions_type_sort":4},{"questions_type_id":"7lwm3-2tvqsg","questions_type_text":"456","questions_type_sort":11271}]}

export default App;
