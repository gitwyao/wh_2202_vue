import { useState, useEffect } from 'react'
import axios from '../http'

// 自定义Hook中可以使用内置hooks

function useTitle() {
    document.title = "设置标题"
}

function useCount(state) {
    const [count, setCount] = useState(state)
    return [count, setCount]
}

function useOptions(api) {
    const [list, setList] = useState([])
    useEffect(() => {
        axios.get(api).then(res => {
            setList(res.data.data)
        })
    }, [])
    return list
}

export {
    useTitle,
    useCount,
    useOptions
}