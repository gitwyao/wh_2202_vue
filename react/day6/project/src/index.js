import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import Provid from './components4/Provid'


ReactDOM.render(
  // <Provid>
  //   <App />
  // </Provid>,
  <App />,
  document.getElementById('root')
);

