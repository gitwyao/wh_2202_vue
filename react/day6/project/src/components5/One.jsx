import React, { useContext } from 'react'
import { Context } from '../components4/Provid'

export default function One() {
    const { state, dispatch } = useContext(Context)
    return (
        <div>
            One --- { state.count }
        </div>
    )
}
