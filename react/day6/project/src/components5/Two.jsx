import React,  { useContext } from 'react'
import { Context } from '../components4/Provid'

export default function Two() {
    const { state, dispatch } = useContext(Context)
    return (
        <div>
            Two --- { state.count }
            <button onClick={() => {
                dispatch({ type: "CHANGE" })
            }}>点我</button>
        </div>
    )
}
