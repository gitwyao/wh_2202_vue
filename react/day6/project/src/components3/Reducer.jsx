import React, { useReducer } from 'react'

const reducer = (state, { type }) => {
    let NewState = JSON.parse(JSON.stringify(state))
    switch(type) {
        case "CHANGE":
            NewState.name = "李四"
            return NewState
    }
}

const initState = {
    name: "张三",
    age: 20
}

export default function Reducer() {
    const [state, dispatch] = useReducer(reducer, initState)
    return (
        <div>
            <h4>
                姓名: { state.name }
            </h4>
            <h4>
                年龄: { state.age }
            </h4>

            <button onClick={() => {
                dispatch({ type: "CHANGE" })
            }}>修改姓名</button>
        </div>
    )
}
