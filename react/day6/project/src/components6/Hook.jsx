import React from 'react'
import { useTitle, useCount } from '../hooks'
// 自定义hooks  ==>  HOC

export default function Hook() {
    const [a] = useCount(500)
    return (
        <div>
            Hook - { a }
        </div>
    )
}
