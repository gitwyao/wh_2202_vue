import React from 'react'
import { useCount } from '../hooks'

export default function Hook2() {
    const [b, setb] = useCount(800)
    return (
        <div>
            Hook2 - { b }
            <button onClick={() => {
                setb(7890)
            }}>setb</button>
        </div>
    )
}
