import React, { useState } from 'react'
// hooks  所有的hook只能在函数式组件中来使用

// useState  定义内部状态

export default function Com() {
    const [count, setCount] = useState(200)
    const [info, setInfo] = useState({
        name: "张三",
        age: 20
    })
    return (
        <div>
            {/* Com -- {count}
            <button onClick={() => {
                setCount(300)
            }}>修改</button> */}

            <h3>
                姓名: {info.name}
            </h3>
            <h3>
                年龄: {info.age}
            </h3>

            <button onClick={() => {
                setInfo({
                    ...info,
                    name: "李四"
                })
            }}>修改</button>
        </div>
    )
}

