import React from 'react'
import { useEffect } from 'react'

export default function Destroy() {
    // 销毁期
    useEffect(() => {
        return () => {
            console.log('我被销毁了')
        }
    })
    return (
        <div>Destroy</div>
    )
}
