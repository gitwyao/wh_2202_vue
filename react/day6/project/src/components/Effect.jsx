import React, { useEffect, useState, memo, PureComponent } from 'react'
// 实例期
// 存在期  state  props
// 销毁期

function Effect(props) {
    const [count, setCount] = useState(888)
    const [num, setNum] = useState(1000)

    // function init() {
    //     console.log('初始化')
    // }

    // useEffect(callback, [])
    // 如果第二个参数是空数组 只执行一次
    useEffect(() => {
        console.log(111)
    })

    // useEffect(() => {
    //    console.log('我是count修改要执行的逻辑')
    // }, [count])

    // useEffect(() => {
    //     console.log('我是num修改要执行的逻辑')
    //  }, [num])

    useEffect(() => {
        console.log('props进行修改了')
     }, [props.count])

    return (
        <div>
            Effect
            <div>
                {count}
                <button onClick={() => {
                    setCount(999)
                }}>点我</button>
            </div>

            <div>
                {num}
                <button onClick={() => {
                    setNum(num + 1)
                }}>点我</button>
            </div>

            <div>
                <h3>
                    props -- { props.count }
                </h3>
            </div>
        </div>
    )
}


export default memo(Effect)