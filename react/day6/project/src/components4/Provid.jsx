import React, { useReducer, createContext } from 'react'

export const Context = createContext()

const reducer = (state, { type }) => {
    let NewState = JSON.parse(JSON.stringify(state))
    switch(type) {
        case "CHANGE":
            NewState.count += 1
            return NewState
    }
}

const initState = {
    count: 8888
}

export default function Provid(props) {
    const [state, dispatch] = useReducer(reducer, initState)
    return (
        <Context.Provider value={{
            state,
            dispatch
        }}>
            { props.children }
        </Context.Provider>
    )
}
