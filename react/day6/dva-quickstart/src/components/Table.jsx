import React from 'react'
import { connect } from 'react-redux'
import { Table, Tag } from 'antd';
const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (text) => <a>{text}</a>,
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    }
];

export const MyTable = ({ list }) => {
    return (
        <Table columns={columns} dataSource={list} />
    )
}

const mapStateToProps = (state) => {
    const { home } = state
    const { list } = home
    return {
        list
    }
}

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(MyTable)