import React from 'react'
import { connect } from 'react-redux'
import { Button, Upload } from 'antd'
import Table from '../components/Table'
import ExportJsonExcel from 'js-export-excel'
import XLSX from 'xlsx'
// @0.16.8

const props = {
    name: 'file',
    // 上传地址
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    // 规定上传文件的类型
    accept: '.xlsx'
    // onChange(info) {

    // },
};


export const HomePage = ({ homeLoading, list, setList }) => {
    const beforeUpload = (file) => {
        return new Promise(reslove => {
            if (reslove) analysis(file)
        })
    }

    // 开始解析excel
    const analysis = (file) => {
        return new Promise((reslove, reject) => {
            // FileReader 异步读取文件
            let fileRead = new FileReader()
            // 设置加载文件
            fileRead.onload = (excel) => {
                console.log('开始加载文件的内容了', excel)
                let data = excel.target.result
                // 是excel文件里的内容
                let excelObj = XLSX.read(data, { type: 'binary' })
                // 获取当前页的名称
                let SheetNames = excelObj.SheetNames[0]
                let excelInfo = excelObj.Sheets[SheetNames]
                let arr = XLSX.utils.sheet_to_json(excelInfo)
                setList(arr)
                reslove()
            }
            // 为了执行onload
            fileRead.readAsBinaryString(file)
        })
    }

    function exportExcel() {
        var toExcel = new ExportJsonExcel({
            // 文件名称
            fileName: "table",
            // 数据
            datas: [
                {
                    // 当前页的名称
                    sheetName: '第一页----',
                    //表头数据
                    sheetHeader: ['姓名', '年龄', "住址"],
                    sheetFilter: ['name', 'age', 'address'],
                    //数据
                    sheetData: list,
                    // 列宽
                    columnWidths: ['8', '4', '8'],
                }
            ]
        });
        toExcel.saveExcel()
    }
    return (
        <div>
            {/* {
                homeLoading? <img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01098e554b5932000001bf72ac2c84.gif&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657848711&t=60339e90b39df0ae6f4cccd34b53bcf6" alt="" />: num
            }
            
            <Button type="primary" onClick={ add }>+1</Button> */}

            <div>
                <Button onClick={exportExcel}>导出表格</Button>
                <Upload beforeUpload={beforeUpload} {...props}>
                    <Button type="primary">导入表格</Button>
                </Upload>
                <Table />
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    const { home, loading } = state
    const { list } = home
    return {
        homeLoading: loading.models.home,
        list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setList(arr) {
            dispatch({ type: "home/setList", arr })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)