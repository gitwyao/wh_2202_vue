import { getList } from '../services/example'

function Copy(arr) {
    return JSON.parse(JSON.stringify(arr))
}

export default {
    namespace: 'home',
    state: {
        list: [
            {
                key: '1',
                name: '张三',
                age: 32,
                address: 'New York No. 1 Lake Park'
            },
            {
                key: '2',
                name: 'Jim Green',
                age: 42,
                address: 'London No. 1 Lake Park'
            },
            {
                key: '3',
                name: 'Joe Black',
                age: 32,
                address: 'Sidney No. 1 Lake Park'
            }
        ],
        num: 100
    },
    reducers: {
        add(state, action) {
            let NewState = Copy(state)
            NewState.num += 1
            return NewState
        },
        setList(state, { arr }) {
            let NewState = Copy(state)
            NewState.list = arr
            console.log(NewState)
            return NewState
        }
    },
    effects: {
        *asyncAdd(obj, { put, call }) {
            let data = yield call(getList)
            // console.log(data)
            yield put({ type: "add" })
        }
    },
    subscriptions: {
        // auth({ dispatch, history }) {
        //     dispatch
        // },
    },
}