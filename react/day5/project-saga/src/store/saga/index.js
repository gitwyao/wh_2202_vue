import { takeEvery } from 'redux-saga/effects'
import { changeList } from './sagas/list'
import { changeNum } from './sagas/num'

function* rootSaga() {
    // while (true) {
        yield takeEvery("ASYNC_ADD_NUM", changeNum)
        yield takeEvery("ASYNC_CHANGE_LIST", changeList)
    // }
}

export default rootSaga