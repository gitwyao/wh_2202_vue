import { delay, put } from 'redux-saga/effects'

function* changeNum() {
    yield delay(1000)
    yield put({ type: 'ADD_NUM' })
}

export {
    changeNum
}