import { call, put } from 'redux-saga/effects'
import axios from 'axios'

function* changeList() {
    // call()
    const data = yield call(axios.get, 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata')
    yield put({ type: "SET_LIST", list: data.data.message })
}

export {
    changeList
}