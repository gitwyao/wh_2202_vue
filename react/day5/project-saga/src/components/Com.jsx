import React, { Component } from 'react'
import { connect } from 'react-redux'

export class Com extends Component {
    componentDidMount() {
        this.props.init()
    }
    render() {
        const { num, add, del, list, init } = this.props
        console.log(list)
        return (
            <div>
                <button  onClick = { del }>---</button>
                { num }
                <button onClick = { add }>+++</button>
                <div>
                    <button onClick={ init }>发起请求</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { num, list } = state
    return {
        num,
        list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add() {
            dispatch({ type: "ASYNC_ADD_NUM" })
        },
        del() {
            dispatch({ type: "DEL_NUM" })
        },
        init() {
            dispatch({ type: "ASYNC_CHANGE_LIST" })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Com)