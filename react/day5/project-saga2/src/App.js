import Com from './components/Com';
import './App.css';

function App() {
  return (
    <div className="App">
      <Com/>
    </div>
  );
}

export default App;
