import { createStore, applyMiddleware } from 'redux'
import createSaga from 'redux-saga'
import rootSaga from './saga'

const saga = createSaga()

let reducer = (state, { type }) => {
    let NewState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "ADD_COUNT":
            NewState.count += 1
            return NewState
        case "DEL_COUNT":
            NewState.count -= 1
            return NewState
        case "CHANGE_LOADING":
            NewState.loading = true
            return NewState
        case "CHANGE_FALSE_LOADING":
            NewState.loading = false
            return NewState
        default:
            return state
    }
}

let initState = {
    count: 100,
    loading: false,
    list: []
}

let store = createStore(reducer, initState, applyMiddleware(saga))

saga.run(rootSaga)

export default store