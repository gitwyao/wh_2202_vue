import { delay, put } from 'redux-saga/effects'

function* changeCount() {
    yield put({ type: "CHANGE_LOADING"})
    yield delay(1500)
    yield put({ type: "ADD_COUNT" })
    yield put({ type: "CHANGE_FALSE_LOADING"})
}

export {
    changeCount
}