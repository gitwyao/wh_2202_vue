import { takeEvery } from 'redux-saga/effects'
import { changeCount } from './sagas/count'

function* rootSaga() {
    // takeEvery(type, *fn)
    yield takeEvery("ASYNC_ADD_COUNT", changeCount)
}

export default rootSaga