import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Spin } from 'antd'

export class Com extends Component {
    render() {
        const { count, add, del, loading } = this.props
        return (
            <div>
                <button onClick = { del }>----</button>
                {
                    loading? <Spin />: count 
                }
                
                <button onClick = { add }>++++</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { count, loading } = state
    return {
        count,
        loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add() {
            dispatch({ type: "ASYNC_ADD_COUNT" })
        },
        del() {
            dispatch({ type: "DEL_COUNT" })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Com)