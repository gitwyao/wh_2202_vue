import { getSwiperdata } from '../services/example'

// put call
// 原生  call(axios.get, url)
// dva  call

export default {
    // 命名空间
    namespace: 'home',
    state: {
        count: 100,
        list: []
    },
    reducers: {
        add(state) {
            state.count += 1
            return { ...state }
        },
        list(state, { data }) {
            console.log('__')
            let NewState = JSON.parse(JSON.stringify(state))
            NewState.list = data
            return NewState
        }
    },
    // 通过redux-saga的方式来处理异步任务
    effects: {
        *asyncAdd(info, { put, call }) {
            console.log('准备处理异步任务')
            let data = yield call(getSwiperdata)
            data = data.data.message
            yield put({ type: "list", data })
        }
    },
    subscriptions: {
        // abc() {
        //     console.log('aaa')
        // },
        // bbb() {
        //     console.log('bbb')
        // },
        setup({ dispatch, history }) {
            dispatch({ type: "asyncAdd" })
            if (history.location.pathname === '/home') {
                alert('准备拦截')

            }
            console.log(history)
        }
    }
}


  