import React, { useEffect } from 'react'
import { connect } from 'dva'
import request from '../utils/request'

// console.log(request('https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata'))


function HomePage(props) {
    console.log(props)
    const { count, dispatch, list } = props
    // useEffect(() => {

    // }, [])
    return (
        <div>
            HomePage -- {count}
            {/* <button onClick={() => {
                dispatch({ type: "home/add" })
            }}>点击</button> */}

            <button onClick={() => {
                dispatch({ type: "home/asyncAdd" })
            }}>异步点击</button>
            {
                list.map((item, index) => {
                    return <h4 key={index}>
                        { item.image_src }
                    </h4>
                })
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    const { home } = state
    const { count, list } = home
    return {
        count,
        list
    }
}

HomePage = connect(mapStateToProps)(HomePage)

export default HomePage
