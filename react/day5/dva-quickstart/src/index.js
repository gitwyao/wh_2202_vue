import dva from 'dva';
import './index.css';

// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/example').default);
// 为了添加数据
app.model(require('./models/home').default);
app.model(require('./models/lisi').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');
