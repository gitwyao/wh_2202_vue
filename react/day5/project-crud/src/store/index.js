import { createStore, combineReducers, applyMiddleware } from 'redux'
import list from './reducer/list'
import thunk from 'redux-thunk'

const reducer = combineReducers({
    list
})

const store = createStore(reducer, applyMiddleware(thunk))

export default store