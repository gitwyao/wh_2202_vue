import { INIT_LIST } from '../actions'

function list(state = [], action) {
    let NewState = JSON.parse(JSON.stringify(state))
    switch(action.type) {
        case INIT_LIST:
            NewState = action.list
            return NewState
        default:
            return state
    }
}

export default list