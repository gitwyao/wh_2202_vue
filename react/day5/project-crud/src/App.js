import Crud from './components/Crud';
import './mock'
import './App.css';

function App() {
  return (
    <div className="App">
      <Crud/>
    </div>
  );
}

export default App;
