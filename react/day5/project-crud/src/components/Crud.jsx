import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, Button, Space, Tag } from 'antd'
import axios from 'axios';
import { INIT_LIST } from '../store/actions'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (text) => <a>{text}</a>,
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Action',
        key: 'action',
        render: (_, record) => (
            <Space size="middle">
                <a>Invite {record.name}</a>
                <a>Delete</a>
            </Space>
        ),
    },
];

export class Crud extends Component {
    componentDidMount() {
        this.props.init()
    }
    render() {
        const { list } = this.props
        return (
            <div>
                <Table columns={columns} dataSource={list} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { list } = state
    return {
        list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        init() {
            dispatch((next) => {
               axios.get('/getList').then(res => {
                    next({ type: INIT_LIST, list: res.data.list })
               }) 
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Crud)