// react-redux  把redux中的数据添加到组件的props上
// 不需要通过setState来进行更新
import { createStore } from 'redux'

let reducer = (state, { type, str }) => {
    let NewState = JSON.parse(JSON.stringify(state))
    switch(type) {
        case "CHANGE_STR":
            NewState.str = str
            return NewState
        default:
            return state
    }
}

let initState = {
    str: "我是redux中的字符串",
    num: 888
}

let store = createStore(reducer, initState)

export default store
