import store from './store';
import { CHANGE_NUM } from './store/actions'
import Com from './components/Com';
import './App.css';

function App() {
  return (
    <div className="App">
      {/* App -- { store.getState().num }
      <button onClick={() => {
        store.dispatch({ type: CHANGE_NUM })
      }}></button> */}

      <Com/>
    </div>
  );
}

export default App;
