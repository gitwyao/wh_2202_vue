import { createStore, combineReducers } from 'redux'
import count from './reducer/count'
import num from './reducer/num'

// 自定义拆分
// let reducer = (state, { type }) => {
//     return {
//         count: count(state.count, type),
//         num: num(state.num, type)
//     }
// }

// let initState = {
//     count: 100,
//     num: 888
// }


// combineReducers 拆分
let reducer = combineReducers({
    count,
    num
})

let store = createStore(reducer)

export default store
