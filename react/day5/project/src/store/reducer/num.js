import { CHANGE_NUM } from '../actions'

function num(state = 200, type) {
    switch (type) {
        case CHANGE_NUM:
            state += 10
            return state
        default:
            return state 
    }
}

export default num