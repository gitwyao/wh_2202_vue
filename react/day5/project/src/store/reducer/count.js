import { CHANGE_COUNT } from '../actions'

function count(state = 100, type) {
    switch (type) {
        case CHANGE_COUNT:
            state += 1
            return state
        default:
            return state
    }
}

export default count