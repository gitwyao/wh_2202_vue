import React, { Component } from 'react'
import { connect } from 'react-redux'

class Com extends Component {
    render() {
        console.log(this.props)
        const { str, change } = this.props
        return (
            <div>
                Com -- {str}
                <div>
                    {/* <button onClick={ () => {
                        dispatch({ type: "CHANGE_STR" })
                    } }>点我</button> */}

                    <button onClick = { () => {
                        change('bbb')
                    } }>点我</button>
                </div>
            </div>
        )
    }
}


// 映射State去当前组件的props上
const mapStateToProps = (state) => {
    const { str } = state
    return {
        str
    }
}

// 映射修改数据的方法去props上
// 使用 mapDispatchToProps 后 this.props上就没有dispatch
const mapDispatchToProps = (dispatch) => {
    return {
        change(str) {
            dispatch({ type: "CHANGE_STR", str })
        }
    }
}

Com = connect(mapStateToProps, mapDispatchToProps)(Com)

// connect  柯里化函数

// console.log(connect)

// function fn(a, b) {
//     return a + b
// }

// fn(aaaaaaaaaa, bbbbbbbbbbb)

// function fn(a) {
//     return (b) => {
//         return a + b
//     }
// }

// fn(1000)(2000)

export default Com
