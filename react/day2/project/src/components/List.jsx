import React, { Component } from 'react'
import axios from 'axios'

export default class List extends Component {
    state = {
        list: []
    }
    timer = null
    constructor(props) {
        super(props)
    }

    // componentWillMount() {

    // }

    render() {
        const { list } = this.state
        return (
            <div>
                {/* {
                    list.map((item, index) => {
                        return <img key={index} src={ item.image_src } alt="" />
                    })
                } */}
                <h3>
                    List组件
                </h3>
            </div>
        )
    }

    componentDidMount() {
        // axios.get('https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata').then(res => {
        //     console.log(res.data.message)
        //     // 存在期
        //     // render
        //     this.setState({
        //         list: res.data.message
        //     })
        // })


        this.timer = setInterval(() => {
            console.log('定时任务')
        }, 1000)
    }

    componentWillUnmount() {
        // 清空副作用
        console.log('组件被销毁了')
        clearInterval(this.timer)
    }
}
