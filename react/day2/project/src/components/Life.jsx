import React, { Component } from 'react'

export default class Life extends Component {
    state = {
        count: 8888
    }
    // constructor(props) {
    //     super(props)
    //     console.log('实例期第一个--constructor')
    // }

    // 组件  将要  挂载
    // componentWillMount() {
    //     console.log('实例期第二个--componentWillMount')
    // }

    add = () => {
        this.setState({
            count: this.state.count += 1
        })
    }

    render() {
        const { count } = this.state
        console.log('实例期第三个--render')
        return (
            <div>
                Life --- { count }
                <button onClick={ this.add }>+++</button>
            </div>
        )
    }

    // 组件 完成  挂载
    // componentDidMount() {

    // }


    // 组件   应该   更新 ?
    shouldComponentUpdate() {
        // 如果return  false  直接终止掉   不会执行下面的生命周期
        return true
    }


    // 组件  将要  更新
    componentWillUpdate() {
        console.log('数据将要更新---2')
    }

    // 组件  完成  更新
    componentDidUpdate() {
        console.log('数据完成更新---4')
    }

}
