import React, { Component } from 'react'
import Son from './Son'

export default class Father extends Component {
    state = {
        count: 100
    }
    render() {
        console.log('父组件数据更新了~~')
        const { count } = this.state
        return (
            <div>
                我是父组件 --- {count}
                <button onClick={() => {
                    this.setState({
                        count: this.state.count += 1
                    })
                }}>点我</button>

                <Son/>
                {/* <Son count={count}/> */}
            </div>
        )
    }
}
