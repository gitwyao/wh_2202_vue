import React, { Component, PureComponent } from 'react'

export default class Son extends Component {
    state = {
        aaa: "aaa",
        count: 10
    }

    // 组件   将要    接收  props
    // componentWillReceiveProps(props) {
    //     // console.log(props)
    // }

    // shouldComponentUpdate(props, state) {
    //     // console.log(props, state)
    //     // 父组件给子组件传值
    //     if (Object.keys(props).length > 0) {
    //         return true
    //     } else {
    //         return false
    //     }
    // }

    componentWillUpdate(props, state) {
        console.log(props, state)
    }
    render() {
        console.log('props修改3')
        const { count } = this.props
        return (
            <div>
                Son -- { count }
            </div>
        )
    }

    componentDidUpdate(prevProps, state) {
        console.log('props修改4')
        console.log(prevProps, state)
    }
}
