import React from 'react'
import Life from './components/Life'
import Father from './components/Father';
import List from './components/List';
import './App.css'

// function App() {
//   return (
//     <div className="App">
//       {/* <Life/> */}

//       {/* <Father/> */}

//       {/* 发起请求 */}
//       <List/>
//     </div>
//   )
// }

class App extends React.Component {
  state = {
    flag: true
  }
  render() {
    const { flag } = this.state
    return <div>
      { flag? <List/>: <></> }
      
      <button onClick={() => {
        this.setState({
          flag: !this.state.flag
        })
      }}>点我</button>
    </div>
  }
}


export default App;
