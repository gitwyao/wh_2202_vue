import React, { Component, createContext, Fragment } from 'react'
import Father from './Father'
import { Provider } from '../utils/context'

// Provider  提供数据   (想要给谁提供数据，使用Provider包裹一下)
// Consumer  使用数据  
// const { Provider, Consumer } = createContext()
// {
//     Provider,
//     Consumer
// }

class GrandFather extends Component {
    state = {
        num: 9999
    }
    render() {
        const { num } = this.state
        return (
            <div>
                <h1>
                    GrandFather -- { num }
                </h1>
                <button onClick={() => {
                    this.setState({
                        num: this.state.num += 1
                    })
                }}>+++</button>
                <Provider value = { num }>
                    <Father/>
                </Provider>
            </div>
        )
    }
}


export default GrandFather
