import React, { Component,  createContext, Fragment } from 'react'
// const { Provider, Consumer } = createContext()
import { Consumer } from '../utils/context'

// {
//     Provider,
//     Consumer
// }

export default class Son extends Component {
    render() {
        return (
            <div>
                {/* Consumer  中 必须要写一个函数 */}
                <Consumer>
                    {
                        (value) => {
                            return <Fragment>
                                Son--{ value }
                            </Fragment>
                        }
                    }
                </Consumer>
            </div>
        )
    }
}