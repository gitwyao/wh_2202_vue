import React, { Component } from 'react'

export default class Event extends Component {
    state = {
        num: 888
    }
    fn = () => {
        // 合成事件
        // 16版本  把事件添加到document
        // 17版本  把事件添加到 #root



        // setState   如果是合成事件 => 异步的
        // setState   如果是原生事件 => 同步的

        // this.setState({
        //     num: 999
        // })
        // console.log(this.state.num)
    }
    render() {
        const { num } = this.state
        // 避免垃圾回收
        // 浏览器的兼容
        
        // eventLoop
        // 微任务  宏任务
        // promise
        // setTimeout(() => promise)
        // 同步
        
        return (
            <div>
                {/* <h3>
                    {num}
                </h3>
                <button onClick={this.fn}>点我</button> */}
                {/* <button id='btn'>点我原生</button> */}


                <div id='one' onClick={() => {
                    alert('橘黄色')
                }} className='one'>
                    <div id='two' onClick={() => {
                        alert('天蓝色')
                    }} className='two'>
                        <div id='three' onClick={() => {
                            alert('红色')
                        }} className='three'>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
    componentDidMount() {
        // let btn = document.getElementById('btn')
        // btn.addEventListener('click', () => {
        //     this.setState({
        //         num: 99999
        //     })
        //     console.log(this.state.num)
        // })


        // let one = document.getElementById('one')
        // let two = document.getElementById('two')
        // let three = document.getElementById('three')

        // one.addEventListener('click', () => {
        //     alert('橘黄色')
        // })

        // two.addEventListener('click', () => {
        //     alert('天蓝色')
        // }, true)

        // three.addEventListener('click', () => {
        //     alert('红色')
        // })
    }
}
