import React, { Component } from 'react'
import { high } from '../high'

class High2 extends Component {
    render() {
        const { count, add } = this.props
        return (
            <div>
                High2-- { count }
                <button  onClick = { add } >++</button>
            </div>
        )
    }
}

High2 = high(High2, 1000, 100)

export default High2
