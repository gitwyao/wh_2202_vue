import React, { Component, createRef } from 'react'

export default class Com extends Component {
    state = {
        ref: createRef()
    }
    // abc = null
    render() {
        return (
            <div>
                Com
                {/* <h3 ref="abc">我是标签</h3> */}
                {/* <h3 ref={(ref) => {
                    this.abc = ref
                }}>我是标签</h3> */}
                <h3 ref={ this.state.ref }>我是标签</h3>
            </div>
        )
    }
    componentDidMount() {
        // ref
        // 1. ref='abc'  在严格模式下或报错
        // 2. ref='fn'
        // 3. createRef 
        // console.log(this.state.ref.current)
    }
}
