import React, { Component } from 'react'
import { high } from '../high'

class High extends Component {
    render() {
        const { count, add } = this.props
        return (
            <div>
                {/* HOC---高阶组件 */}
                {/* 高阶函数  函数  接收一个函数作为参数   或者  返回一个函数 */}
                {/* 高阶组件  函数  接收一个组件作为参数   并且  返回一个新的组件*/}
                High-- { count } 
                <button onClick = { add }>++</button>
            </div>
        )
    }
}

High = high(High, 500, 10)

export default High
