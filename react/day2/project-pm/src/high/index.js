import React, { Component } from 'react'
{/* 高阶组件  函数  接收一个组件作为参数   并且  返回一个新的组件*/ }

// 高阶组件
// 1. 属性代理
// 2. 反向继承
function high(Com, count = 888, num) {
    return class NewCom extends Component {
        state = {
            count
        }

        add = () => {
            this.setState({
                count: this.state.count += num
            })
        }
        render() {
            const { count } = this.state
            return <div className='father'>
                <Com count={count} add={this.add} />
            </div>
        }
    }
}



function drag(Com, top = 0, left = 0) {
    return class NewCom extends Component {
        state = {
            top,
            left
        }

        onMouseDown = (ev) => {
            // console.log("按下")
            // let clientX = ev.clientX
            // let clientY = ev.clientY

            window.addEventListener('mousemove', this.MouseMove)
            window.addEventListener('mouseup', () => {
                console.log('抬起')
                window.removeEventListener('mousemove', this.MouseMove)
                // window.removeEventListener('mouseup')
            })
        }

        MouseMove = (ev) => {
            let left = ev.clientX
            let top = ev.clientY
            this.setState({
                top,
                left
            })
        }
        render() {
            const { top, left } = this.state
            return <div className='ps' style={{
                top,
                left
            }} onMouseDown={this.onMouseDown}>
                <Com />
            </div>
        }
    }
}


export {
    high,
    drag
}