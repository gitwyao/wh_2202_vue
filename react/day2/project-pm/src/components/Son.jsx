import React, { Component } from 'react'

export default class Son extends Component {
    state = {
        count: 888
    }
    render() {
        const { count } = this.state
        const { sendNum } = this.props
        return (
            <h5>
                我是子组件 -- { count }
                <button onClick={ () => {
                    sendNum(count)
                } }>点我</button>
            </h5>
        )
    }
}
