import React, { Component } from 'react'
import Son from './Son'

export default class Father extends Component {
    state = {
        num: null
    }
    sendNum = (num) => {
        this.setState({
            num
        })
    }
    render() {
        const { num } = this.state
        return (
            <div>
                <h2>
                    我是父组件---{ num }
                </h2>
                <Son sendNum={this.sendNum} />
            </div>
        )
    }
}
