// import Father from './components/Father';
// import GrandFather from './components2/GrandFather';
// import Com from './components3/Com';
// import Event from './components3/Event';
// import High from './components3/High'
// import High2 from './components3/High2';

import One from './components4/One';
import Two from './components4/Two';

import './App.css';

function App() {
  return (
    <div className="App">
      {/* <Father/> */}
      {/* 嵌套传值 */}
      {/* <GrandFather/> */}

      {/* <Com/> */}

      {/* <Event/> */}

      {/* <High/>
      <High2/> */}

      <One/>
      <Two/>
    </div>
  );
}

export default App;
