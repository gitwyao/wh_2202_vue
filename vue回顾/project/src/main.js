import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './mock'
import ElementUI from 'element-ui'
import en from 'element-ui/lib/locale/lang/en'
import ch from 'element-ui/lib/locale/lang/zh-CN'
import ru from 'element-ui/lib/locale/lang/ru-RU'

import VueI18n from'vue-i18n'
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(VueI18n) // 通过插件的形式挂载，通过全局⽅法 Vue.use() 使⽤插件

const messages = {
  'en': {
    message: 'hello',
    ...en // 或者用 Object.assign({ message: 'hello' }, enLocale)
  },
  'zh': {
    message: '你好',
    ...ch // 或者用 Object.assign({ message: '你好' }, zhLocale)
  },
  'ru': {
    message: '不认识',
    ...ru // 或者用 Object.assign({ message: '你好' }, zhLocale)
  }
}

const i18n = new VueI18n({
  locale: 'zh', // 语⾔标识 //this.$i18n.locale // 通过切换locale的值来实现语⾔切换
  messages
})
Vue.config.productionTip = false

Vue.use(ElementUI)

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
