export default {
    // 开启命名空间
    namespaced: true,
    mutations: {
        fn(state, name) {
            state.name = name
        }
    },
}