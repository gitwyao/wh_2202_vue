import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: "王五"
  },
  getters: {
  },
  mutations: {
    setName(state, name) {
      state.name = name
    },
    setName2(state, name) {
      state.name = name
    }
  },
  actions: {
    init(context) {
      setTimeout(() => {
        context.commit("setName2", "我是异步任务获取的数据")
      }, 2000)
    }
  },
  modules: {
      // a
  }
})
