import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home'
import About from '../views/About'

// 二级路由
import PageA from '../views/home/PageA'
import PageB from '../views/home/PageB'


Vue.use(VueRouter)


const router = new VueRouter({
    // 路由表  (存放可以切换的页面)
    routes: [
        // 路由重定向
        {
            path: "/",
            redirect: "/home"
        },
        {
            //  url地址
            path: "/home",
            // 匹配的组件
            component: Home,
            // 配置子路由
            children: [
                // {
                //     path: "/home",
                //     redirect: "/home/a"
                // },
                {
                    path: "/home/a",
                    component: PageA
                },
                {
                    path: "/home/b",
                    component: PageB
                },
            ]
        },
        {
            //  url地址
            path: "/about",
            // 匹配的组件
            component: About
        }
    ],
    // 设置路由模式  (默认 hash)
    // hash      #   
    // history   /
    mode: "history",
    // 前缀
    base: "/"
})


const RouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
  return RouterPush.call(this, to).catch(err => err)
}

export default router