import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// import VueRouter from 'vue-router'

// import Home from './views/Home'
// import About from './views/About'



// 路由
// vue-router

// Vue.use(VueRouter)

//  1. npm i vue-router@3 -S
//  2. Vue.use(VueRouter)
//  3. 在new VueRouter中配置路由表
//  4. 把VueRouter 配置到 Vue中



// const router = new VueRouter({
//   // 路由表  (存放可以切换的页面)
//   routes: [
//     {
//       //  url地址
//       path: "/home",
//       // 匹配的组件
//       component: Home
//     },
//     {
//       //  url地址
//       path: "/about",
//       // 匹配的组件
//       component: About
//     }
//   ]
// })



// Vue.directive('abc', () => {
//   console.log('全局自定义指令')
// })

new Vue({
  render: h => h(App),
  // router  用来匹配路由
  router, 
}).$mount('#app')
