import axios from 'axios'

let baseURL = ""

if (process.env.NODE_ENV === "development") {
    baseURL = "192.168.92"
} else if (process.env.NODE_ENV === "production") {
    baseURL = "api-hmugo-web.itheima.net"
}

axios.defaults.baseURL = baseURL
