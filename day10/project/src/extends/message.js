import Message from './Message.vue'
let messageInfo = {}

messageInfo.install = (Vue) => {
    let Com = Vue.extend(Message)
    // 获取到当前组件实例
    let vm = new Com()
    // 获取到当前的dom结构 
    let el = vm.$mount().$el
    // 添加到页面中
    document.body.appendChild(el)

    // 给this上添加方法
    Vue.prototype.$message = ({ message, type, duration = 3000 }) => {
        vm.text = message
        vm.className = type
        vm.flag = true
        // 定时消失
        setTimeout(() => {
            vm.flag = false
        }, duration)
    }

    // console.log(vm.$mount().$el)
}


export default messageInfo