import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import message from './extends/message'
import axios from 'axios'
import './js'


Vue.config.productionTip = false

// Vue.use()  注册的组件上要有一个install
Vue.use(message)

Vue.prototype.$axios = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
