    // 1. 创建.vue文件模板
    // 2. 在同级目录下创建.js文件控制
    // 3. 创建一个对象，添加install方法， 方法中第一个参数为Vue
    // 4. 通过Vue.extend(Com)来创建一个“子类”  (构造函数  VueComponent)
    // 5. let $vm = new VueComponent() ===> 代替的是Com  实例
    // 6. 获取模板(节点)   $vm.$mount().$el
    // 7. 通过document.body.appendChild   添加到页面
    // 8. $vm.flag = true
    // 9. 因为调用的时候是通过this.$message  Vue.prototype.$message = () => {}
    // 10. 定义变量$开头  @each $list: (a: sd)