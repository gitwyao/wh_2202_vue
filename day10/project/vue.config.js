console.log(process.env.NODE_ENV)

module.exports = {
    devServer: {
        // 代理
        proxy: {
            "/api": {
                // 目标地址
                target: "http://localhost:7001",
                // 是否跨域
                changeOrigin: true,
                // http://localhost:7001/user/login
                // /api/api/user/login
                pathRewrite: {
                    "^/api": ""
                }
            }
        }

        // proxy: "http://localhost:7001"
    }
}