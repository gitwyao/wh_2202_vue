import Vue from 'vue'
import App from './App.vue'
import mixin from './mixins'
// 图片懒加载插件
import LazyLoad from 'vue-lazyload'

Vue.config.productionTip = false

// 全局混入
// Vue.mixin(mixin)

// 全局定义自定义指令
// Vue.directive('指令名称')

// 使用第三方插件想要在vue中使用，必须use
Vue.use(LazyLoad, {
  // 加载中的图片
  loading: "https://img0.baidu.com/it/u=666320934,1539077995&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500",
  // 设置错误图片
  error: "https://img2.baidu.com/it/u=1821554556,1569508556&fm=253&fmt=auto&app=138&f=JPEG?w=600&h=500"
})

Vue.directive('def', (el, option) => {
  console.log('我是全局的自定义指令')
})

new Vue({
  render: h => h(App),
}).$mount('#app')
