import Vue from 'vue'
import App from './App.vue'
// import Vuex from 'vuex'
import store from './store'

Vue.config.productionTip = false

// Vue.use(Vuex)

// Vuex@3

// 1. Vue.use(Vuex)
// 2. 实例
// 3. 

// let store = new Vuex.Store({
//   // 定义全局的状态
//   state: {
//     num: 456789
//   }
// })

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
