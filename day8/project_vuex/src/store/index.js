import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)


let store = new Vuex.Store({
    // 定义全局的状态
    state: {
        num: 678,
        list: []
    },
    // 同步的方式来修改数据
    mutations: {
        // 在mutations中定义的函数，会有两个默认参数
        // 1. state
        // 2. 传递的参数
        changNum(state, { num }) {
            // console.log(action)
            state.num = num
            // console.log('执行', state)
        },
        // 经过异步请求后，对list重新赋值
        setList(state, action) {
            state.list = action.list
        }
    },
    // 类似.vue中的计算属性  computed
    getters: {
        NewNum(state) {
            // 必须返回内容
            // console.log('我是vuex中getters')
            return state.num + '@@@@@@'
        }
    },
    // 只要是异步  必须要放入到actions  (是为了执行异步任务)
    actions: {
        // 在actions中定义的函数，默认参数是上下文
        init(context) {
            axios.get("https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata").then(res => {
                context.commit("setList", {
                    list: res.data.message
                })
                console.log(context)

                // this.commit("setList", {
                //     list: res.data.message
                // })
            })
            // console.log('我是actions中的方法')
            // 
        }
    }
})

export default store