import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// @ => src
import Classify from '@/views/home/Classify'
import Car from '@/views/home/Car'
import My from '@/views/home/My'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "/home"
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: 'classify',
        component: Classify
      },
      {
        path: 'car',
        component: Car
      },
      {
        path: 'my',
        component: My
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
