import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 列表数据
    list: [],
    // 购物车数据
    carList: []
  },
  mutations: {
    // 对List进行赋值
    setList(state, list) {
      state.list = list
    },
    // 点击首页商品添加到购物车
    addCarItem(state, item) {
      // 判断购物车有无当前商品
      // 如果没有 直接添加
      let index = state.carList.findIndex(items => items.id === item.id)
      if (index === -1) {
        state.carList.push(item)
      } else {
        state.carList[index].count += 1
      }
    },
    // 购物车中的数据添加
    addCarItemCount(state, index) {
      state.carList[index].count += 1
    },
    // 购物车中的数据减少
    delCarItemCount(state, index) {
      // 如果大于1可以正常减少
      if (state.carList[index].count > 1) {
        state.carList[index].count -= 1
      } else {
        // 等于1直接删除
        state.carList.splice(index, 1)
      }
    },
    // 把购物车中的选中状态改变成全选一样的
    changeChecked(state, flag) {
      state.carList.forEach(item => {
        item.checked = flag
      })
    }
  },
  actions: {
    // 执行异步任务
    // context 
    init({ commit }) {
      axios.get("/getCarList").then(res => {
        // 拿到数据对List进行赋值
        commit("setList", res.data.data)
      })
    }
  },
  getters: {
    // 计算总数量
    allCount({ carList }) {
      // 2   true
      // 3   true
      // 4   false

      // 0 + 2
      // 2 + 3
      // 5 + 4
      return carList.reduce((prev, next) => {
        if (next.checked) {
          // 选中
          return prev + next.count
        } else {
          // 没有选中
          return prev
        }
      }, 0)
    },
    allPrice({ carList }) {
      return carList.reduce((prev, next) => {
        if (next.checked) {
          // 选中
          return prev + next.count * next.price
        } else {
          // 没有选中
          return prev
        }
        
      }, 0)
    }
  },
  modules: {
  }
})
