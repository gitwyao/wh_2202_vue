import Mock from 'mockjs'

const data = [
    {
        id: 0,
        img: "https://img.dmallcdn.com/20190318/70f0c2bb-0633-4fce-a0ae-1a54cd766bed_480x480H",
        title: "缤纷田园进口橙 约800g",
        price: 15,
        count: 1,
        checked: true
    },
    {
        id: 1,
        img: "https://img.dmallcdn.com/20220415/2f976507-fe8c-4e67-8412-23802e60358a_480x480H",
        title: "进口加力果盒装 约2kg",
        price: 10,
        count: 1,
        checked: true
    },
    {
        id: 2,
        img: "https://img.dmallcdn.com/20190316/e2759d12-eb2a-443b-98b1-4237ff1ca5f7_480x480H",
        title: "新疆库尔勒香梨 约1kg/袋",
        price: 25,
        count: 1,
        checked: true
    }
]

Mock.mock("/getCarList", {
    data
})