import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 把接口在项目中引入
import './mock'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
