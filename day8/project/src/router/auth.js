export default [
    {
        path: "/home",
        component: "Home的页面",
        meta: {
            auth: ["管理员", "老师", "学生"]
        },
        children: [
            {
                path: "/home/one",
                meta: {
                    auth: ["管理员", "老师"]
                }
            },
            {
                path: "/home/two",
                meta: {
                    auth: ["管理员"]
                },
            }
        ]
    },
    {
        path: "/my",
        component: "My的页面",
        meta: {
            auth: ["管理员", "老师"]
        },
        children: [
            {
                path: "/my/info",
                meta: {
                    auth: ["管理员", "老师"]
                },
            }
        ]
    },
    {
        path: "/about",
        component: "About的页面",
        meta: {
            auth: ["管理员"]
        },
        children: [
            {
                path: "/about/test",
                meta: {
                    auth: ["管理员"]
                },
            },
            {
                path: "/about/pro",
                meta: {
                    auth: ["管理员"]
                },
            }
        ]
    }
]