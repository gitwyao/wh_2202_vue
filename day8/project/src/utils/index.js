function filterRoutes(arr, test) {
    let res = arr.filter(item => item.meta.auth.indexOf(test) !== -1)

    // let res1 = [1, 2, 3, 4, 5]

    // console.log(res1.filter(item => item === 3))
    // console.log(res1)

    // 对子路由筛选

    // 如果筛选的数据length > 0

    res.length > 0 && res.forEach(item => {
        // 判断当前这一项下是否有children
        if (item.children) {
            // 把筛选出来的数据赋值给children
            item.children = filterRoutes(item.children, test)
        }
    })

    return res
}


export {
    filterRoutes
}