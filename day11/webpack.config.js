const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // 要打包的入口文件 (.js   .json)
    entry: "./main.js",
    // 出口文件  (绝对路径)
    // __dirname  当前文件的父级的绝对路径
    output: {
        // 文件夹的目录
        path: __dirname + "/dist",
        // 文件的名称
        filename: "main.js"
    },
    // 设置打包模式
    //  'development' or 'production'
    mode: "production",
    // 装载器
    module: {
        rules: [
            {
                test: /\.css$/,
                // 'style-loader', 'css-loader'   顺序
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(jpg|png|gif)$/,
                // 只是为了可以打包
                // use: "file-loader"
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // name  文件的名称
                            // path  父级路径
                            // name  文件的名称
                            // ext  后缀
                            name: '[path][name].[ext]',
                        }
                    }
                ]
            },
            // {
            //     test: /\.vue$/,
            //     use: 'vue-loader'
            // },
            // .vue  vue-loader   vue-template-compiler
            // css文件  js  分离
        ]
    },
    // 插件
    plugins: [
        // 插件都需要new 
        new HtmlWebpackPlugin({
            // template  基于的模板
            template: './public/index.html'
        })
    ]
}