import axios from 'axios'
import { reactive, onMounted, ref } from 'vue'

 function useTable(url) {
    let tableData = reactive({
        arr: []
    })

    // let tableData = ref([])
    onMounted(() => {
        axios.get(url).then(res => {
            // res.data.data  不是响应式
            tableData.arr = res.data.data
            // tableData.value = res.data.data
        })
    })

    return {
        tableData
    }
}

export {
    useTable
}
