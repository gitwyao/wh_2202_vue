import { ref } from 'vue'
function useCount(num = 888) {
    const count = ref(num)
    const add = () => {
        count.value += 1
    }
    return {
        count,
        add
    }
}

export {
    useCount
}