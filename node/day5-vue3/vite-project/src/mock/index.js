import Mock from 'mockjs'

let data = [
    {
        date: "2016-05-03",
        name: "Tom111",
        address: "No. 189, Grove St, Los Angeles",
    },
    {
        date: "2016-05-02",
        name: "Tom",
        address: "No. 189, Grove St, Los Angeles",
    },
    {
        date: "2016-05-04",
        name: "Tom",
        address: "No. 189, Grove St, Los Angeles",
    },
    {
        date: "2016-05-01",
        name: "Tom",
        address: "No. 189, Grove St, Los Angeles",
    },
];



let data2 = [
    {
        date: "2016-05-03",
        name: "张三",
        address: "No. 189, Grove St, Los Angeles",
    },
    {
        date: "2016-05-02",
        name: "李四",
        address: "No. 189, Grove St, Los Angeles",
    },
    {
        date: "2016-05-04",
        name: "王五",
        address: "No. 189, Grove St, Los Angeles",
    }
];

Mock.mock('/one', {
    data
})

Mock.mock('/two', {
    data: data2
})