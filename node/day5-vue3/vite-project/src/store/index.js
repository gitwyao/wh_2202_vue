import { createStore } from 'vuex'

export default createStore({
    state: {
        num: 888
    },
    getters: {

    },
    mutations: {
        addNum(state) {
            state.num += 100
        }
    }
})