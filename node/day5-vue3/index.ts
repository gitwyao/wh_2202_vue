// 约束数据类型  (基本类型)
let str: string = "str"
let num: number = 100
let flag: boolean = true

// undefined  和  null  是所有类型的子类
let und: undefined = undefined
let nulls: null = null

let any: any = true

// 联合类型
let str2: string | number = 123


// (引用类型)

// 接口 (约束对象的类型)
interface IUserInfo {
    name: string,
    age: number,
    sex: boolean,
    md?: string
}

let userInfo: IUserInfo = {
    name: "张三",
    age: 28,
    sex: true,
    md: "说明"
}


// 枚举(在一个已知的范围中, 选取其中的一个)

// enum Direction {
//     top,
//     bottom,
//     left,
//     right
// }

enum Direction {
    Top = "top",
    Bottom = "bottom",
    Left = "left",
    Right = "right"
}



// 数组 (每一项的类型都一样)
// 1. (约定个数)
// let arr: [number] = [1]
// 2. (不约定个数)
// let arr: Array<number> = [1, 2, 3]

// 元组 
// let temp: [number, string] = [111, 'sss']
// let temp: Array<number | string> = [111, 'sss']


// 函数
// void  返回内容为空
function add(): number {
    return 123
}

add()


// 基本泛型
// function fn(arg) {
//     return arg
// }

// function fn<T>(arg: T): T {
//     return arg
// }

// let str3 = fn('str')

// 约束性泛型 (提前告诉程序)
interface Ilength {
    length: number
}

function fn<T extends Ilength>(arg: T): T {
    console.log(arg.length)
    return arg
}

let str3 = fn('str')
// console.log(str3.length)