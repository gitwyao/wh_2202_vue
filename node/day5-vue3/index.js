// 约束数据类型  (基本类型)
var str = "str";
var num = 100;
var flag = true;
// undefined  和  null  是所有类型的子类
var und = undefined;
var nulls = null;
// 联合类型
var str2 = 123;
var userInfo = {
    name: "张三",
    age: 28,
    sex: true,
    md: "说明"
};
// 枚举(在一个已知的范围中, 选取其中的一个)
// enum Direction {
//     top,
//     bottom,
//     left,
//     right
// }
var Direction;
(function (Direction) {
    Direction["Top"] = "top";
    Direction["Bottom"] = "bottom";
    Direction["Left"] = "left";
    Direction["Right"] = "right";
})(Direction || (Direction = {}));
// 数组 (每一项的类型都一样)
// 1. (约定个数)
// let arr: [number] = [1]
// 2. (不约定个数)
// let arr: Array<number> = [1, 2, 3]
// 元组 
// let temp: [number, string] = [111, 'sss']
// let temp: Array<number | string> = [111, 'sss']
// 函数
// void  返回内容为空
function add() {
    return 123;
}
add();
