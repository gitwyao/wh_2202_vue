const express = require('express')
const bodyParser = require('body-parser')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const { auth, authority } = require('./utils')
const sqlGetData = require('./db')

const app = express()
const port = 3000
// 中间件

// 自定义中间件  添加  Authorization字段
var allowCors = function (req, res, next) {
    res.header("Access-Control-Expose-Headers", "Authorization");
    next();
}
app.use(allowCors);

// 使用插件
app.use(bodyParser.json())


// 注册
app.post('/register', async (req, res) => {
    const { userName, passWord, auth } = req.body
    // 判断用户是否存在
    let data = await sqlGetData(`select * from user where name='${userName}'`)

    if (data.length > 0) {
        res.send({
            code: -1,
            msg: "该账号已存在"
        })
    } else {
        // 加密
        const md5 = crypto.createHash('md5')
        const hash = md5.update(passWord).digest('hex')
        await sqlGetData(`insert into user (name, password, userId) values ('${userName}', '${hash}', ${auth})`)
        // 插入数据库
        res.send({
            code: 1,
            data: '注册成功'
        })
    }
})

// 登录
app.post('/login', async (req, res) => {
    const { userName, passWord } = req.body
    // 判断用户是否存在
    // 加密
    const md5 = crypto.createHash('md5')
    const hash = md5.update(passWord).digest('hex')
    let data = await sqlGetData(`select * from user where name='${userName}' and password='${hash}'`)
    let userId = data[0].userId
    if (data.length > 0) {
        let token = jwt.sign({
            userId,
            passWord,
            userName
        }, 'key', {
            expiresIn: '1h'
        })
        res.send({
            code: 1,
            msg: '登录成功',
            token
        })
    } else {
        // 账号密码不匹配
        res.send({
            code: -1,
            msg: '账号密码不匹配'
        })
    }
})


// 获取用户信息
app.get('/getUserInfo', auth, (req, res) => {
    // 通过token 解析出用户id
    const token = req.headers.authorization
    jwt.verify(token, 'key', async (err, datas) => {
        // data.userName  用户名称
        let data = await sqlGetData(`select * from userinfo where name='${datas.userName}'`)
        console.log(data)
        res.send({
            code: 1,
            msg: "获取用户信息成功",
            data
        })
    })
})

// 获取列表
app.get('/getList', auth, async (req, res) => {
    let data = await sqlGetData(`select * from list`)
    res.send({
        code: 1,
        msg: "获取列表成功",
        data
    })
})

// 删除接口
app.delete("/delItem/:id", auth, authority, async (req, res) => {
    await sqlGetData(`Delete from list where id=${req.params.id}`)
    res.send({
        code: 1,
        msg: "删除成功"
    })
})




// 使用中间件
// app.get('/list', auth, authority, (req, res) => {
//     res.send('12345678666')
// })



// get  /list
// 请求方式  (请求地址)
// app.get('/list', (req, res) => {
//     // 
//     console.log(req.query)
//     res.send('12345678')
// })


// post  /login
// app.post('/login', (req, res) => {
//     res.send({
//         data: '登录接口'
//     })
// })


// post  /login  有参数
// app.post('/login', (req, res) => {
//     console.log(req.body)
//     res.send({
//         data: '登录接口'
//     })
// })


// delete
// app.delete('/del/:id', (req, res) => {
//     console.log(req.params)
//     res.send({
//         data: '删除接口'
//     })
// })

// // put 
// app.put('/edit/:idd', (req, res) => {
//     console.log(req.params)
//     res.send({
//         data: '编辑接口'
//     })
// })


app.listen(port, () => {
    console.log('express启动--' + port)
})
