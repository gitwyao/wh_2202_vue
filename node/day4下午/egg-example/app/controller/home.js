'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }

  async lists() {
    const { ctx } = this;
    ctx.body = await ctx.service.getList.index()
  }
}

module.exports = HomeController;
