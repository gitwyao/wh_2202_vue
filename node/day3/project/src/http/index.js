import axios from 'axios'
import { Message } from 'element-ui'

axios.interceptors.request.use((config) => {
    const token = sessionStorage.getItem('token')? sessionStorage.getItem('token'): ''
    config.headers.Authorization = token
    return config
})

axios.interceptors.response.use((data) => {
    console.log(data.data.code)
    // 业务逻辑为正确
    // if (data.data.code === 1) {
    //     Message.success(data.data.msg)
    // } else {
    //     Message.error(data.data.msg)
    // }
    return data.data
})

export default axios