const jwt = require('jsonwebtoken')

const auth = async (req, res, next) => {
    const token = req.headers.authorization
    if (token) {
        next()
    } else {
        res.send({
            msg: "请先登录",
            code: -1
        })
    }
}

const authority = (req, res, next) => {
    const token = req.headers.authorization
    jwt.verify(token, 'key', (err, data) => {
        console.log(data.userId)
        if (data.userId === 2) {
            res.send({
                code: -1,
                msg: "权限不足"
            })
        } else {
            next()
        }
    })
}

module.exports = {
    auth,
    authority
}