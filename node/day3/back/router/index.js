const express = require('express')
const router = express.Router()
const sqlGetData = require('../db')
const { auth } = require('../utils')

router.get('/appraisal', auth, async (req, res) => {
    const { punctuality, work, review, textarea, ip, name } = req.query
    // 插入到数据库
    let data = await sqlGetData(`insert into ceping (punctuality, work, review, textarea, ip, name) values (${punctuality},${work},${review}, '${textarea}', '${ip}', '${name}')`)
    console.log(data)
    res.send({
        code: 1,
        msg: "评价成功"
    })
})

router.get('/search', auth, async (req, res) => {
    let data = await sqlGetData('select * from ceping')
    res.send({
        code: 1,
        msg: "查询成功",
        data
    })
})


module.exports = router

