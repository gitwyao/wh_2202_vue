const mysql = require('mysql')

const con = mysql.createPool({
    user: 'root',
    password: 'root',
    host: 'localhost',
    database: '1_crud'
})

function sqlGetData(sql) {
    return new Promise((res, rej) => {
        con.query(sql, (err, data) => {
            if (err) {
                rej(err)
            } else {
                res(data)
            }
        })
    })
}

module.exports = sqlGetData