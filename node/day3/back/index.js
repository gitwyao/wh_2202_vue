const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const indexRouter = require('./router/index')
const userRouter = require('./router/user')

app.use(bodyParser.json())


app.use('/', indexRouter)
app.use('/user', userRouter)


app.listen(3000, () => {
    console.log('启动成功')
})


