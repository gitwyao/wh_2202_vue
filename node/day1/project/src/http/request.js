import axios from 'axios'

axios.interceptors.request.use((config) => {
    console.log(config)
    // 获取token
    let token = sessionStorage.getItem('token')? sessionStorage.getItem('token'): ''
    config.headers.Authorization = token
    return config
})

axios.interceptors.response.use((data) => {
    return data
})

export default axios