// 内置模块
const http = require('http')
const url = require('url')

const mysql = require('mysql')

// /list?id=3
// url.parse("转换的内容")
console.log(url.parse("/list?id=3&abc=asd", true))


// 连接数据池
const con = mysql.createPool({
    // 用户名称
    user: "root",
    // 密码
    password: "root",
    // 主机
    host: "localhost",
    // 数据库名称
    database: "1_crud"
})

// 使用sql  con.query(sql, callback)
// con.query('select * from list', (err, data) => {
//     // err  错误信息
//     // data 查询到的信息
//     console.log(data)
// })



// npm i nodemon -g

http.createServer((req, res) => {
    // 查看请求方式  GET  POST
    // console.log(req.method)
    // 查看请求路径
    // console.log(req.url)

    // console.log('我现在启动了后端服务')
    // 设置响应的内容

    // 设置响应头
    res.writeHead(200, {
        "Content-Type": "text/plain; charset=UTF-8"
    })

    // 区分请求方式
    if (req.method === 'GET') {
        // 区分地址
        console.log(req.url)
        if (url.parse(req.url, true).pathname === '/list') {
            // res.end('我是list的数据')
            // res.end  如果响应的引用数据 需要转换为字符串
            // 取参数
            const { id } = url.parse(req.url, true).query

            con.query(`select * from list where id=${id}`, (err, data) => {
                // err  错误信息
                // data 查询到的信息
                res.end(JSON.stringify({
                    code: 1,
                    msg: "请求列表数据成功",
                    data
                }))
            })   
        } else {
            res.end('icon')
        }
    } else {
        console.log('POST请求')
        res.end('1234567')
    }
    // res.end('默认')
}).listen(3000)