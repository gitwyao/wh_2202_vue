const mysql = require('mysql')

// 连接数据池
const con = mysql.createPool({
    // 用户名称
    user: "root",
    // 密码
    password: "root",
    // 主机
    host: "localhost",
    // 数据库名称
    database: "1_crud"
})

// 查询
// select * from list    查询全部
// select * from list where id=1    按条件来进行查找
// select name,age from list   根据列名来查找


// 插入
// insert into list (name, address, age) values ('王五', '武汉', ${30})   单行插入
// insert into list (name, address, age) values ('赵六', '武汉', ${30}), ('冯七', '武汉', ${20})       多行插入



con.query(`insert into list (name, address, age) values ('赵六', '武汉', ${30}), ('冯七', '武汉', ${20})`, (err, data) => {
    console.log(err)
})

